package com.food.registration.provider;


import android.util.Log;

import com.food.R;
import com.food.core.service.PDHAPIProviderBase;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHStringUtil;
import com.food.core.utils.PDHURLUtils;
import com.food.registration.model.RegistrationRequestModel;
import com.food.registration.model.RegistrationResponseModel;
import com.food.registration.service.RegistrationAPIService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by hemendra.kumar
 */
public class RegistrationProvider  extends PDHAPIProviderBase {


    public static boolean lastRequestSuccess = true;

    private RegistrationRequestModel registrationRequest;


    public RegistrationProvider(RegistrationRequestModel registrationRequest) {
        this.registrationRequest = registrationRequest;
    }

    @Override
    public void loadDataFromServer() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appContext.getString(R.string.host_url))
                .addConverterFactory(converterFactory)
                .build();
        RegistrationAPIService service = retrofit.create(RegistrationAPIService.class);
        String jsonData = PDHURLUtils.JSONData(registrationRequest, RegistrationRequestModel.class, PDHAppState.getInstance().encodeInputJSON);
        Log.d("API:Req:getMTNDetails", jsonData);
        Call<RegistrationResponseModel> call = service.getRegistration(registrationRequest);
        final RegistrationResponseModel model = null;
        call.enqueue(new Callback<RegistrationResponseModel>() {
            @Override
            public void onResponse(Response<RegistrationResponseModel> response, Retrofit var) {
                RegistrationResponseModel model = response.body();
                String errorString = null;
                if (null == model) {
                    errorString = "INVALID Response:Check Connection";
                } else if (!PDHConstants.SERVER_SUCCESS_CODE.equals(model.Result.ErrorCode)) {
                    Log.e("API:Resp:getMTNDetails", model.toString());

                    errorString = PDHStringUtil.createServerErrorString(model.Result.ErrorCode, model.Result.ErrorMsg, null);
                    model = null;
                }
                callback.onResponse(errorString, model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("app", "error" + t.toString());
                RegistrationProvider.lastRequestSuccess = false;
                callback.onResponse(t.toString(), null);
            }
        });
    }


}
