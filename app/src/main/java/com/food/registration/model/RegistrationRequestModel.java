package com.food.registration.model;

/**
 * Created by agu186 on 2/20/2016.
 */
public class RegistrationRequestModel {
    private String name;
    private String email;
    private String mobile;
    private String username;
    private String password;

    public RegistrationRequestModel(String name, String email, String mobile,String username, String password ) {
        this.name =name;
        this.email=email;
        this.mobile =mobile;
        this.username = username;
        this.password = password;
    }
}
