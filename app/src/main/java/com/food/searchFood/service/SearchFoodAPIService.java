/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.searchFood.service;

import com.food.categoryMenuList.model.CategoryListResponseModel;
import com.food.categoryMenuList.model.MenuListResponseModel;
import com.food.orderHistory.model.OrderHistoryRequestModel;
import com.food.orderHistory.model.OrderHistoryResponseModel;
import com.food.searchFood.model.SearchFoodModel;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by shaimu8 on 12/15/15.
 */
public interface SearchFoodAPIService {

    @GET("/restAppNew/api/menu/search.json")
    Call<MenuListResponseModel> getSearchAPI(@Query("keyword") String order);

}
