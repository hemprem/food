package com.food.searchFood.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;

/**
 * Created by agu186 on 2/20/2016.
 */
public class SearchFoodModel extends PDHAPIDataModelBase {

@SerializedName("Result")
@Expose
public Output Result;


public class Output {
    @SerializedName("ErrorCode")
    @Expose
    public String ErrorCode;
    @SerializedName("ErrorMsg")
    @Expose
    public String ErrorMsg;

    @SerializedName("Record")
    @Expose
    public ArrayList<RecordList> Record;

    public class RecordList {
        @SerializedName("Id")
        @Expose
        public String Id;

        @SerializedName("Title")
        @Expose
        public String Title;

        @SerializedName("HalfPrice")
        @Expose
        public int HalfPrice;

        @SerializedName("FullPrice")
        @Expose
        public int FullPrice;

        @SerializedName("DishOfWeek")
        @Expose
        public int DishOfWeek;

        @SerializedName("DishType")
        @Expose
        public String DishType;

        @SerializedName("Image")
        @Expose
        public String Image;
    }
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
