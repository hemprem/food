package com.food.categoryMenuList.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;

/**
 * Created by agu186 on 2/20/2016.
 */
public class CategoryListResponseModel extends PDHAPIDataModelBase {

@SerializedName("Result")
@Expose
public Output Result;


public class Output {
    @SerializedName("ErrorCode")
    @Expose
    public String ErrorCode;
    @SerializedName("ErrorMsg")
    @Expose
    public String ErrorMsg;

    @SerializedName("Record")
    @Expose
    public ArrayList<RecordList> Record;

    public class RecordList {
        @SerializedName("Id")
        @Expose
        public String Id;

        @SerializedName("Name")
        @Expose
        public String Name;
        @SerializedName("Image")
        @Expose
        public String Image;
    }
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
