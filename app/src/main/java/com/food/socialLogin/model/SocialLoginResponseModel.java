package com.food.socialLogin.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by agu186 on 2/20/2016.
 */
public class SocialLoginResponseModel extends PDHAPIDataModelBase {

@SerializedName("Result")
@Expose
public Output Result;

public class Output {
    @SerializedName("ErrorCode")
    @Expose
    public String ErrorCode;
    @SerializedName("ErrorMsg")
    @Expose
    public String ErrorMsg;

    @SerializedName("User")
    @Expose
    public UserDetails  User;

    public class UserDetails {
        @SerializedName("id")
        @Expose
        public String id;

        @SerializedName("name")
        @Expose
        public String name;

        @SerializedName("email")
        @Expose
        public String email;

        @SerializedName("mobile")
        @Expose
        public String mobile;
    }

 }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
