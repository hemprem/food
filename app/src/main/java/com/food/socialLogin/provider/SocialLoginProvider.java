package com.food.socialLogin.provider;


import android.util.Log;

import com.food.R;
import com.food.core.service.PDHAPIProviderBase;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHStringUtil;
import com.food.core.utils.PDHURLUtils;
import com.food.socialLogin.model.SocialLoginRequestModel;
import com.food.socialLogin.model.SocialLoginResponseModel;
import com.food.socialLogin.service.SocialLoginAPIService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by agu186 on 2/20/2016.
 */
public class SocialLoginProvider extends PDHAPIProviderBase {


    public static boolean lastRequestSuccess = true;

    private String selectedMtn = null;
    private Boolean loanInfoRequired = null;
    private Boolean fullInfoRequired = null;
    private SocialLoginRequestModel socialLoginRequest;

    public String jsonName = null;

    public SocialLoginProvider(String selectedMtn, Boolean loanInfoRequired, Boolean fullInfoRequired) {
        this.selectedMtn = selectedMtn;
        this.loanInfoRequired = loanInfoRequired;
        this.fullInfoRequired = fullInfoRequired;
    }


    public SocialLoginProvider(SocialLoginRequestModel socialLoginRequest) {
     this.socialLoginRequest = socialLoginRequest;
    }

    @Override
   public void loadDataFromServer() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appContext.getString(R.string.host_url))
                .addConverterFactory(converterFactory)
                //.client(PDHHTTPClientFactory.getVZWHTTPClient(false))  //client(PDHHTTPClientFactory.getVZWHTTPClientAddCookiesToHeader(false))   //client(PDHHTTPClientFactory.getVZWHTTPClient(true))
                .build();
        SocialLoginAPIService service = retrofit.create(SocialLoginAPIService.class);
        String jsonData = PDHURLUtils.JSONData(socialLoginRequest, SocialLoginRequestModel.class, PDHAppState.getInstance().encodeInputJSON);
        Log.d("API:Req:getMTNDetails", jsonData);
        Call<SocialLoginResponseModel> call = service.getLogin(socialLoginRequest);
        call.enqueue(new Callback<SocialLoginResponseModel>() {
            @Override
            public void onResponse(Response<SocialLoginResponseModel> response, Retrofit var) {
                SocialLoginResponseModel model = response.body();
                String errorString = null;
                if (null == model) {
                    errorString = "INVALID Response:Check Connection";
                } else if (!PDHConstants.SERVER_SUCCESS_CODE.equals(model.Result.ErrorCode)) {
                    Log.e("API:Resp:getMTNDetails", model.toString());

                    errorString = PDHStringUtil.createServerErrorString(model.Result.ErrorCode, model.Result.ErrorMsg, null);
                    model = null;
                }
                callback.onResponse(errorString, model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("app", "error" + t.toString());
                SocialLoginProvider.lastRequestSuccess = false;
                callback.onResponse(t.toString(), null);
            }
        });
    }



    @Override
    public void loadDataFromCache() {
        try {
            InputStream is = appContext.getAssets().open("jsons/getMTNDetails.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String bufferString = new String(buffer);
            Gson gson = new Gson();
            SocialLoginResponseModel model = gson.fromJson(bufferString, SocialLoginResponseModel.class);
            callback.onResponse(null, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
