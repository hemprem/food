/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.socialLogin.service;

import com.food.socialLogin.model.SocialLoginRequestModel;
import com.food.socialLogin.model.SocialLoginResponseModel;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by shaimu8 on 12/15/15.
 */
public interface SocialLoginAPIService {

    //@FormUrlEncoded
    @Headers({
            "Accept: application/json",
            "Host: bemisaal.in"
    })

    @POST("/restAppNew/api/siteUsers/socialLogin.json")
    Call<SocialLoginResponseModel> getLogin(@Body SocialLoginRequestModel socialLoginRequestModel);

}
