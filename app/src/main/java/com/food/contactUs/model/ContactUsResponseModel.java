package com.food.contactUs.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by agu186 on 2/20/2016.
 */
public class ContactUsResponseModel extends PDHAPIDataModelBase {

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("url")
    @Expose
    public String url;

    @SerializedName("code")
    @Expose
    public String code;
}
