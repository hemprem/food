package com.food.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.food.R;
import com.food.activity.AddressBookActivity;
import com.food.address.model.GetAddressListResponseModel;

import java.util.ArrayList;

/**
 * Author by hemendra.kumar on 2/24/2016.
 */
public class AddressListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<GetAddressListResponseModel.Output.RecordList> addressModelItems;

    public AddressListAdapter(Context context, ArrayList<GetAddressListResponseModel.Output.RecordList> addressModelItems) {
        this.context = context;
        this.addressModelItems = addressModelItems;
    }



    @Override
    public int getCount() {
        return addressModelItems.size();
    }

    @Override
    public Object getItem(int position) {
        return addressModelItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.address_list_item, parent, false);

        TextView txtPersonName=(TextView)view.findViewById(R.id.txtPersonName);
        TextView txtAddressLineOne=(TextView)view.findViewById(R.id.txtAddressLineOne);
        TextView txtMobNo=(TextView)view.findViewById(R.id.txtMobNo);

        ImageButton imgBtnDelete =(ImageButton)view.findViewById(R.id.btnDeleteItem);
        ImageButton imgBtnEdit =(ImageButton)view.findViewById(R.id.btnEdItem);
        Button chooseAddBtn =(Button)view.findViewById(R.id.chooseAdd_Btn);

        if(position==0){
            chooseAddBtn.setPressed(true);
            chooseAddBtn.setActivated(false);
           // chooseAddBtn.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pdh_button_pressed));
        } else {
            chooseAddBtn.setPressed(false);
            chooseAddBtn.setActivated(true);
           // chooseAddBtn.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pdh_button_selector));
        }


        txtPersonName.setText(addressModelItems.get(position).Name);
        txtAddressLineOne.setText(addressModelItems.get(position).Address+" ,"
                +addressModelItems.get(position).Locality+" - "+
                addressModelItems.get(position).Pincode);
        txtMobNo.setText("Mob No -" + addressModelItems.get(position).Mobile);

        imgBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof AddressBookActivity) {

                    final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                    LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = layoutInflater.inflate(R.layout.custom_alert_dialog,null);
                    TextView titleTV = (TextView) view.findViewById(R.id.alert_title_tv);
                    TextView mesgTV = (TextView) view.findViewById(R.id.alert_mesg_tv);
                    titleTV.setText("Address Delete");
                    mesgTV.setText("Are you sure you want to delete this?");
                    Button okBtn = (Button) view.findViewById(R.id.alert_ok_btn);
                    Button  cancelBtn = (Button) view.findViewById(R.id.alert_cancel_btn);

                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            ((AddressBookActivity) context).deleteAddress(position);
                        }
                    });

                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.setView(view);
                    alertDialog.show();

                }
            }
        });
        imgBtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             if(context instanceof AddressBookActivity) {
                 ((AddressBookActivity) context).editAddress(position);
             }
            }
        });

        chooseAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;
    }


    private void deleteDialog(Context ctx ,int position){



    }
}