package com.food.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.food.R;
import com.food.activity.MenuDetailsActivity;
import com.food.cart.model.AddItemToCartRequestModel;
import com.food.cart.model.EditItemToCartRequestModel;
import com.food.cart.provider.AddItemToCartProvider;
import com.food.cart.provider.EditItemToCartProvider;
import com.food.categoryMenuList.model.MenuListResponseModel;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.util.AppPreferences;

import java.util.ArrayList;

/**
 * Author by hemendra.kumar on 2/24/2016.
 */
public class PageOneAdapter extends BaseAdapter {


    private Context context;
    private ArrayList<MenuListResponseModel.Output.RecordList> projectNameModelItems;
    int halfPriceQuantityText =1;
    int fullPriceQuantityText =1;
    SharedPreferences prefs;
    public PageOneAdapter(Context context, ArrayList<MenuListResponseModel.Output.RecordList> projectNameModelItems) {
        this.context = context;
        this.projectNameModelItems = projectNameModelItems;
    }

    @Override
    public int getCount() {
        return projectNameModelItems.size();
    }

    @Override
    public Object getItem(int position) {
        return projectNameModelItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.menu_list_items, parent, false);
        prefs= PreferenceManager.getDefaultSharedPreferences(context);
        projectNameModelItems.get(position);
        TextView tv = (TextView) view.findViewById(R.id.subject_TV);
        tv.setText(projectNameModelItems.get(position).Title);

        TextView mv = (TextView) view.findViewById(R.id.message_TV);
        mv.setText("Rs "+projectNameModelItems.get(position).FullPrice +"/-");

        Button btnAddToCart = (Button) view.findViewById(R.id.timeStampTV);
        final MenuListResponseModel.Output.RecordList record = (MenuListResponseModel.Output.RecordList)projectNameModelItems.get(position);
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                LayoutInflater factory = LayoutInflater.from(context);
                final View deleteDialogView = factory.inflate(
                        R.layout.add_to_cart_layout, null);
                final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
                deleteDialog.setView(deleteDialogView);

                ImageButton addBtnHalfPrice =(ImageButton)deleteDialogView.findViewById(R.id.addBtnHalfPrice);
                ImageButton minusBtnHalfPrice =(ImageButton)deleteDialogView.findViewById(R.id.minusBtnHalfPrice);
                ImageButton addBtnFullPrice =(ImageButton)deleteDialogView.findViewById(R.id.addBtnFullPrice);
                ImageButton minusBtnFullPrice =(ImageButton)deleteDialogView.findViewById(R.id.minusBtnFullPrice);

                final TextView halfPriceTextView =(TextView)deleteDialogView.findViewById(R.id.halfPriceTextView);
                final TextView fullPriceTextView =(TextView)deleteDialogView.findViewById(R.id.fullPriceTextView);

                if(record.HalfPrice!=null) {
                    halfPriceTextView.setText("Half Rs " + record.HalfPrice);
                }
                if(record.FullPrice!=null) {
                    fullPriceTextView.setText("Full Rs " + record.FullPrice);
                }
                final TextView halfPriceQuantityTextView =(TextView)deleteDialogView.findViewById(R.id.halfPriceQuantityTextView);
                halfPriceQuantityTextView.setText(String.valueOf(halfPriceQuantityText));

                final TextView fullPriceQuantityTextView =(TextView)deleteDialogView.findViewById(R.id.fullPriceQuantityTextView);
                fullPriceQuantityTextView.setText(String.valueOf(fullPriceQuantityText));

                TextView addToOrderBtn =(TextView)deleteDialogView.findViewById(R.id.addToOrderBtn);

                addBtnHalfPrice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        halfPriceQuantityText = halfPriceQuantityText+1;
                        halfPriceQuantityTextView.setText(String.valueOf(halfPriceQuantityText));
                    }
                });
                minusBtnHalfPrice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(halfPriceQuantityText>0) {
                            halfPriceQuantityText = halfPriceQuantityText - 1;
                            halfPriceQuantityTextView.setText(String.valueOf(halfPriceQuantityText));
                        }
                    }
                });
                addBtnFullPrice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fullPriceQuantityText = fullPriceQuantityText + 1;
                        fullPriceQuantityTextView.setText(String.valueOf(fullPriceQuantityText));
                    }
                });
                minusBtnFullPrice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(fullPriceQuantityText>0) {
                            fullPriceQuantityText = fullPriceQuantityText - 1;
                            fullPriceQuantityTextView.setText(String.valueOf(fullPriceQuantityText));
                        }
                    }
                });

                addToOrderBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* Intent i = new Intent(context, MyOrderActivity.class);
                        context.startActivity(i);*/

                        addToCart(AppPreferences.getUserEmail(prefs),record.Id, String.valueOf(fullPriceQuantityText), "1", record.FullPrice);
                        deleteDialog.dismiss();
                    }
                });
                deleteDialog.show();
            }
        });

        return view;
    }
    private AddItemToCartRequestModel getAddToCartRequest(String uid, String menuId, String quantity, String size, String price) {
        AddItemToCartRequestModel request = null;
        return new AddItemToCartRequestModel(uid,menuId,quantity,size,price);
    }

    private EditItemToCartRequestModel getEditToCartRequest(String uid, String menuId, String quantity, String size, String price) {
        EditItemToCartRequestModel request = null;
        return new EditItemToCartRequestModel(uid,menuId,quantity,size,price);
    }

    public void addToCart(String uid, String menuId, String quantity, String size, String price){
        AddItemToCartProvider provider = new AddItemToCartProvider(getAddToCartRequest(uid, menuId,quantity,size, price));
        provider.setCallback((MenuDetailsActivity)context);
        provider.appContext = context;
        PDHViewUtils.getInstance().showProgress(context);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void editToCart(String uid, String menuId, String quantity, String size, String price){
        EditItemToCartProvider provider = new EditItemToCartProvider(getEditToCartRequest(uid, menuId,quantity,size, price));
        provider.setCallback((MenuDetailsActivity)context);
        provider.appContext = context;
        PDHViewUtils.getInstance().showProgress(context);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

}