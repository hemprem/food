package com.food.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.food.R;
import com.food.activity.MyOrderActivity;
import com.food.cart.model.GetCartDetailResponseModel;
import com.food.categoryMenuList.model.CategoryListResponseModel;
import com.food.categoryMenuList.model.MenuListResponseModel;

import java.util.ArrayList;

/**
 * Author by hemendra.kumar on 2/24/2016.
 */
public class MyOrderAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<GetCartDetailResponseModel.Output.RecordList> projectNameModelItems;
    public MyOrderAdapter(Context context, ArrayList<GetCartDetailResponseModel.Output.RecordList> projectNameModelItems) {
        this.context = context;
        this.projectNameModelItems = projectNameModelItems;
    }

    @Override
    public int getCount() {
        return projectNameModelItems.size();
    }

    @Override
    public Object getItem(int position) {
        return projectNameModelItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.my_order_item, parent, false);

        GetCartDetailResponseModel.Output.RecordList record =  (GetCartDetailResponseModel.Output.RecordList) projectNameModelItems.get(position);
        TextView txtItem = (TextView) view.findViewById(R.id.txtItem);
        TextView txtItemPrice = (TextView) view.findViewById(R.id.txtItemPrice);
        TextView txtPrice = (TextView) view.findViewById(R.id.txtPrice);
        TextView txtQty = (TextView) view.findViewById(R.id.txtQty);

        txtItem.setText(record.MenuTitle);
        txtItemPrice.setText("Rs. "+record.Price+"");
        txtPrice.setText("Rs. "+record.Price+"");
        txtQty.setText(record.Quantity+"");

        ImageButton btnAddToCart = (ImageButton) view.findViewById(R.id. imgBtnAddQty);
        ImageButton btnMinusToCart = (ImageButton) view.findViewById(R.id.imgBtnMinusQty);
        ImageButton btnDelete = (ImageButton) view.findViewById(R.id.btnDeleteItem);

        return view;
    }
}