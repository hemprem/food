package com.food.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.food.R;
import com.food.orderHistory.model.OrderHistoryResponseModel;
import com.food.profile.model.GetUserProfileResponseModel;

import java.util.ArrayList;

/**
 * Created by Moni gupta on 15-05-2016.
 */
public class OrderHistoryAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<OrderHistoryResponseModel.Output.RecordData> orderHistoryModelItems;

    public OrderHistoryAdapter(Context context, ArrayList<OrderHistoryResponseModel.Output.RecordData> orderHistoryModelItems) {
        this.context = context;
        this.orderHistoryModelItems = orderHistoryModelItems;
    }



    @Override
    public int getCount() {
        if(orderHistoryModelItems!=null){
            return orderHistoryModelItems.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return orderHistoryModelItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.order_history_list_item, parent, false);

        TextView txtOrderItemName=(TextView)view.findViewById(R.id.txtOrderItemName);
        TextView txtOrderTotalPrice=(TextView)view.findViewById(R.id.txtOrderTotalPrice);
        TextView txtOrderDate=(TextView)view.findViewById(R.id.txtOrderDate);

        txtOrderItemName.setText(orderHistoryModelItems.get(position).Comment);
        txtOrderTotalPrice.setText("Rs"+" "+String.valueOf(orderHistoryModelItems.get(position).Total_Price));
        txtOrderDate.setText(orderHistoryModelItems.get(position).Order_Date);

        return view;
    }
}
