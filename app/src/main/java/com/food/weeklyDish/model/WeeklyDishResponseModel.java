package com.food.weeklyDish.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;

/**
 * Author by hemendra.kumar on 4/5/2016.
 */
public class WeeklyDishResponseModel extends PDHAPIDataModelBase {

    @SerializedName("Result")
    @Expose
    public Output Result;


    public class Output {
        @SerializedName("ErrorCode")
        @Expose
        public String ErrorCode;
        @SerializedName("ErrorMsg")
        @Expose
        public String ErrorMsg;

        @SerializedName("Record")
        @Expose
        public ArrayList<RecordList> Record;

        public class RecordList {
            @SerializedName("Id")
            @Expose
            public String Id;
            @SerializedName("Title")
            @Expose
            public String Title;
            @SerializedName("HalfPrice")
            @Expose
            public String HalfPrice;
            @SerializedName("FullPrice")
            @Expose
            public String FullPrice;
            @SerializedName("DishOfWeek")
            @Expose
            public String DishOfWeek;
            @SerializedName("DishType")
            @Expose
            public String DishType;
            @SerializedName("Image")
            @Expose
            public String Image;
        }
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
