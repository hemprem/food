/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.weeklyDish.service;

import com.food.weeklyDish.model.WeeklyDishResponseModel;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by shaimu8 on 12/15/15.
 */
public interface WeeklyDishAPIService {


    @GET("/restAppNew/api/menu/get.json?is_week_dish=1")
    Call<WeeklyDishResponseModel> getWeeklyDishList();
}
