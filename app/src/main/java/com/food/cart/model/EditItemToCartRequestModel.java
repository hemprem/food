package com.food.cart.model;

/**
 * Created by agu186 on 2/20/2016.
 */
public class EditItemToCartRequestModel {
    private String uid;
    private String menu_id;
    private String quantity;
    private String size;
    private String price;


    public EditItemToCartRequestModel(String uid, String menu_id,  String quantity, String size, String price) {
        this.uid = uid;
        this.menu_id = menu_id;
        this.quantity = quantity;
        this.size = size;
        this.price = price;
    }
}
