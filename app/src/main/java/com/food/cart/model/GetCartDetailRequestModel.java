package com.food.cart.model;

import com.food.core.service.PDHAPIDataModelBase;

/**
 * Created by agu186 on 2/20/2016.
 */
public class GetCartDetailRequestModel extends PDHAPIDataModelBase {
    private String uid;


    public GetCartDetailRequestModel(String uid) {
        this.uid = uid;
    }
}
