package com.food.cart.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;

/**
 * Created by agu186 on 2/20/2016.
 */
public class GetCartDetailResponseModel extends PDHAPIDataModelBase {
    @SerializedName("Result")
    @Expose
    public Output Result;


    public class Output {
        @SerializedName("ErrorCode")
        @Expose
        public String ErrorCode;
        @SerializedName("ErrorMsg")
        @Expose
        public String ErrorMsg;

        @SerializedName("TotalItem")
        @Expose
        public String TotalItem;

        @SerializedName("TotalPrice")
        @Expose
        public String TotalPrice;

        @SerializedName("Record")
        @Expose
        public ArrayList<RecordList> Record;

        public class RecordList {
            @SerializedName("Id")
            @Expose
            public int Id;

            @SerializedName("MenuId")
            @Expose
            public int MenuId;

            @SerializedName("MenuTitle")
            @Expose
            public String MenuTitle;

            @SerializedName("Quantity")
            @Expose
            public int Quantity;

            @SerializedName("Size")
            @Expose
            public int Size;

            @SerializedName("Price")
            @Expose
            public int Price;

            @SerializedName("Image")
            @Expose
            public String Image;

        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);

    }
}