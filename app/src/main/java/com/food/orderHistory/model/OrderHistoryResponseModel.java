package com.food.orderHistory.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;

/**
 * Created by agu186 on 2/20/2016.
 */
public class OrderHistoryResponseModel extends PDHAPIDataModelBase {

@SerializedName("Result")
@Expose
public Output Result;

@Override
public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
}
public class Output {

    @SerializedName("ErrorCode")
    @Expose
    public String ErrorCode;
    @SerializedName("ErrorMsg")
    @Expose
    public String ErrorMsg;

    @SerializedName("Record")
    @Expose
    public ArrayList<RecordData> Record;

    public class RecordData {

        @SerializedName("Comment")
        @Expose
        public String Comment;

        @SerializedName("Discount")
        @Expose
        public int Discount;

        @SerializedName("Order_Date")
        @Expose
        public String Order_Date;

        @SerializedName("Order_Id")
        @Expose
        public int Order_Id;

        @SerializedName("Status")
        @Expose
        public String Status;

        @SerializedName("Total_Price")
        @Expose
        public int Total_Price;


        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
 }



}
