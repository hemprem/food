package com.food.address.model;

import com.food.core.service.PDHAPIDataModelBase;

/**
 * Created by agu186 on 2/20/2016.
 */
public class DeleteAddressRequestModel extends PDHAPIDataModelBase {
    private String id;
    private String uid;

    public DeleteAddressRequestModel(String id, String uid) {
        this.id = id;
        this.uid = uid;

    }
}
