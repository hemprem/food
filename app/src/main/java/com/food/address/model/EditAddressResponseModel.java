package com.food.address.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by agu186 on 2/20/2016.
 */
public class EditAddressResponseModel extends PDHAPIDataModelBase {

@SerializedName("Result")
@Expose
public Output Result;


public class Output {
    @SerializedName("ErrorCode")
    @Expose
    public String ErrorCode;
    @SerializedName("ErrorMsg")
    @Expose
    public String ErrorMsg;
   }
}
