package com.food.address.model;

import com.food.core.service.PDHAPIDataModelBase;

/**
 * Created by agu186 on 2/20/2016.
 */
public class GetAddressListRequestModel extends PDHAPIDataModelBase {
    private String uid;

    public GetAddressListRequestModel(String uid) {
        this.uid = uid;

    }
}
