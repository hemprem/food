package com.food.address.provider;


import android.util.Log;

import com.food.R;
import com.food.address.model.AddAddressRequestModel;
import com.food.address.model.AddAddressResponseModel;
import com.food.address.service.AddAddressAPIService;
import com.food.cart.model.AddItemToCartResponseModel;
import com.food.core.service.PDHAPIProviderBase;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHStringUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by agu186 on 2/20/2016.
 */
public class AddToAddressProvider extends PDHAPIProviderBase {


    public static boolean lastRequestSuccess = true;

    private String selectedMtn = null;
    private Boolean loanInfoRequired = null;
    private Boolean fullInfoRequired = null;
    private AddAddressRequestModel loginRequest;
    public String jsonName = null;

    public AddToAddressProvider(AddAddressRequestModel loginRequest) {
        this.loginRequest = loginRequest;
    }

    @Override
    public void loadDataFromServer() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appContext.getString(R.string.host_url))
                .addConverterFactory(converterFactory)
                //.client(PDHHTTPClientFactory.getVZWHTTPClientReadCookiesFromHeader(CategoryListProvider.lastRequestSuccess, false))
                .build();
        AddAddressAPIService service = retrofit.create(AddAddressAPIService.class);
        Call<AddAddressResponseModel> call = service.addAddress(loginRequest);
        call.enqueue(new Callback<AddAddressResponseModel>() {
            @Override
            public void onResponse(Response<AddAddressResponseModel> response, Retrofit var) {
                AddAddressResponseModel model = response.body();
                String errorString = null;
                if (null == model) {
                    errorString = "INVALID Response:Check Connection";
                } else if (!PDHConstants.SERVER_SUCCESS_CODE.equals(model.Result.ErrorCode)) {
                    Log.e("API:Resp:getMTNDetails", model.toString());
                    errorString = PDHStringUtil.createServerErrorString(model.Result.ErrorCode, model.Result.ErrorMsg, null);
                    model = null;
                }
                callback.onResponse(errorString, model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("app", "error" + t.toString());
                AddToAddressProvider.lastRequestSuccess = false;
                callback.onResponse(t.toString(), null);
            }
        });
    }



    @Override
    public void loadDataFromCache() {
        try {
            InputStream is = appContext.getAssets().open("jsons/getMTNDetails.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String bufferString = new String(buffer);
            Gson gson = new Gson();
            AddItemToCartResponseModel model = gson.fromJson(bufferString, AddItemToCartResponseModel.class);
            callback.onResponse(null, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
