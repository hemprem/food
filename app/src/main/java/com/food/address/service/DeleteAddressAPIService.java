/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.address.service;

import com.food.address.model.DeleteAddressRequestModel;
import com.food.address.model.DeleteAddressResponseModel;
import com.food.cart.model.AddItemToCartRequestModel;
import com.food.cart.model.AddItemToCartResponseModel;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by shaimu8 on 12/15/15.
 */
public interface DeleteAddressAPIService {
    @Headers({
            "Accept: application/json",
            "Host: bemisaal.in"
    })
    @POST("/restAppNew/api/address/delete.json")
    Call<DeleteAddressResponseModel> deletedAddress(@Body DeleteAddressRequestModel loginRequestModel);
}
