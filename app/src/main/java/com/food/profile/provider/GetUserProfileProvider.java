package com.food.profile.provider;


import android.util.Log;

import com.food.R;
import com.food.core.utils.PDHAppState;
import com.food.login.model.LoginResponseModel;
import com.food.core.service.PDHAPIProviderBase;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHStringUtil;
import com.food.core.utils.PDHURLUtils;
import com.food.profile.model.GetUserProfileRequestModel;
import com.food.profile.model.GetUserProfileResponseModel;
import com.food.profile.service.GetUserProfileAPIService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by agu186 on 2/20/2016.
 */
public class GetUserProfileProvider extends PDHAPIProviderBase {


    public static boolean lastRequestSuccess = true;

    private GetUserProfileRequestModel loginRequest;

    public String jsonName = null;

    public GetUserProfileProvider(GetUserProfileRequestModel loginRequest) {
     this.loginRequest = loginRequest;
    }

    @Override
   public void loadDataFromServer() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appContext.getString(R.string.host_url))
                .addConverterFactory(converterFactory)
                .build();
        GetUserProfileAPIService service = retrofit.create(GetUserProfileAPIService.class);
        String jsonData = PDHURLUtils.JSONData(loginRequest, GetUserProfileRequestModel.class, PDHAppState.getInstance().encodeInputJSON);
        Log.d("API:Req:getMTNDetails", jsonData);
        Call<GetUserProfileResponseModel> call = service.getLogin(loginRequest);
        final GetUserProfileResponseModel model = null;
        call.enqueue(new Callback<GetUserProfileResponseModel>() {
            @Override
            public void onResponse(Response<GetUserProfileResponseModel> response, Retrofit var) {
                GetUserProfileResponseModel model = response.body();
                String errorString = null;
                if (null == model) {
                    errorString = "INVALID Response:Check Connection";
                } else if (!PDHConstants.SERVER_SUCCESS_CODE.equals(model.Result.ErrorCode)) {
                    Log.e("API:Resp:getMTNDetails", model.toString());

                    errorString = PDHStringUtil.createServerErrorString(model.Result.ErrorCode, model.Result.ErrorMsg, null);
                    model = null;
                }
                callback.onResponse(errorString, model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("app", "error" + t.toString());
                GetUserProfileProvider.lastRequestSuccess = false;
                callback.onResponse(t.toString(), null);
            }
        });
    }



    @Override
    public void loadDataFromCache() {
        try {
            InputStream is = appContext.getAssets().open("jsons/getMTNDetails.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String bufferString = new String(buffer);
            Gson gson = new Gson();
            LoginResponseModel model = gson.fromJson(bufferString, LoginResponseModel.class);
            callback.onResponse(null, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
