package com.food.profile.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by agu186 on 2/20/2016.
 */
public class SetProfileImageResponseModel extends PDHAPIDataModelBase {

@SerializedName("Result")
@Expose
public Output Result;

@Override
public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
}
public class Output {

    @SerializedName("ErrorCode")
    @Expose
    public String ErrorCode;
    @SerializedName("ErrorMsg")
    @Expose
    public String ErrorMsg;

    @SerializedName("Record")
    @Expose
    public RecordData Record;

    public class RecordData {

        @SerializedName("Uid")
        @Expose
        public Long Uid;

        @SerializedName("Name")
        @Expose
        public String Name;

        @SerializedName("Mobile")
        @Expose
        public String Mobile;

        @SerializedName("DOB")
        @Expose
        public String DOB;


        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
 }



}
