package com.food.profile.model;

/**
 * Created by agu186 on 2/20/2016.
 */
public class SetProfileImageRequestModel {
    private String uid;
    private String image;


    public SetProfileImageRequestModel(String uid, String image) {
        this.uid = uid;
        this.image = image;

    }
}
