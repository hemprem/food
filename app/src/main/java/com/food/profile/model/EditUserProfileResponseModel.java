package com.food.profile.model;

import com.food.core.service.PDHAPIDataModelBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;

/**
 * Created by agu186 on 2/20/2016.
 */
public class EditUserProfileResponseModel extends PDHAPIDataModelBase {

@SerializedName("Result")
@Expose
public Output Result;

@Override
public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
}
public class Output {

    @SerializedName("ErrorCode")
    @Expose
    public String ErrorCode;
    @SerializedName("ErrorMsg")
    @Expose
    public String ErrorMsg;


 }
}
