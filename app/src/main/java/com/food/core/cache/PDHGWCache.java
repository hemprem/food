/*
 *   -------------------------------------------------------------------------
 *      PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *      Wireless, Inc. and its affiliates except under written
 *      agreement.
 *
 *      This is an unpublished, proprietary work of Verizon Wireless,
 *      Inc. that is protected by United States copyright laws.  Disclosure,
 *      copying, reproduction, merger, translation,modification,enhancement,
 *      or use by anyone other than authorized employees or licensees of
 *      Verizon Wireless, Inc. without the prior written consent of
 *      Verizon Wireless, Inc. is prohibited.
 *
 *      Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *   -------------------------------------------------------------------------
 */

package com.food.core.cache;

import android.content.ContentValues;
import android.content.Context;

import java.util.HashMap;
import java.util.List;

/**
 * Created by bonamsa on 2/7/16.
 */

public class PDHGWCache {

    public static String mdncache = "gwcache";

    private static PDHCacheDB dbHelper = null;

    private static PDHGWCache sharedInstance;

    private PDHGWCache(Context context) {

        if(dbHelper == null) {
            dbHelper = PDHCacheDB.getInstance(context);
            dbHelper.onCreate(dbHelper.getWritableDatabase());
        }
    }

    public static PDHGWCache getInstance(Context context) {

        if(sharedInstance == null) {
            sharedInstance = new PDHGWCache(context);
        }

        return sharedInstance;
    }

    public HashMap<String, String> getGWContent(String selectionQueryString, String selectedMTN, String showAll) {

        List<HashMap<String, String>> resultSet = dbHelper.executeSelect("select * from gwcache where selectionquerystring='"+selectionQueryString+"'  and selectedmtn='"+selectedMTN+"' and showall='"+showAll+"'", null);

        if(resultSet == null || resultSet.size() == 0) {
            return null;
        }

        HashMap<String,String> row = resultSet.get(0);

        long time = Long.parseLong(row.get("lastrequested"));

        long currentTime = System.currentTimeMillis();

        if((currentTime-time) > PDHCacheDB.CACHE_TTL_MILLISECS) {
            deleteGWContent(selectionQueryString, selectedMTN, showAll);
            return null;
        }
        return row;
    }

    private int deleteGWContent(String selectionQueryString, String selectedMTN, String showAll) {

        dbHelper.executeDelete("delete from gwcache where selectionquerystring='"+selectionQueryString+"'  and selectedmtn='"+selectedMTN+"' and showall='"+showAll+"'");

        return 1;
    }

    public boolean cacheGWContent(String selectionQueryString, String selectedMTN, String showAll , String jsonResponse) {

        ContentValues values = new ContentValues();
        values.put("selectionquerystring",selectionQueryString);
        values.put("selectedmtn",selectedMTN);
        values.put("showall",showAll);
        values.put("jsonresponse",jsonResponse);
        values.put("lastrequested", System.currentTimeMillis());

        return dbHelper.insertRow(mdncache, values);
    }
}
