/*
 *   -------------------------------------------------------------------------
 *      PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *      Wireless, Inc. and its affiliates except under written
 *      agreement.
 *
 *      This is an unpublished, proprietary work of Verizon Wireless,
 *      Inc. that is protected by United States copyright laws.  Disclosure,
 *      copying, reproduction, merger, translation,modification,enhancement,
 *      or use by anyone other than authorized employees or licensees of
 *      Verizon Wireless, Inc. without the prior written consent of
 *      Verizon Wireless, Inc. is prohibited.
 *
 *      Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *   -------------------------------------------------------------------------
 */

package com.food.core.cache;

import android.content.ContentValues;
import android.content.Context;

import java.util.HashMap;
import java.util.List;

/**
 * Created by bonamsa on 2/7/16.
 */

public class PDHSessionHeadersCache {

    public static String sessioncache = "sessioncache";

    private static PDHCacheDB dbHelper = null;

    private static PDHSessionHeadersCache sharedInstance;

    private PDHSessionHeadersCache(Context context) {

        if(dbHelper == null) {
            dbHelper = PDHCacheDB.getInstance(context);
            dbHelper.onCreate(dbHelper.getWritableDatabase());
        }
    }

    public static PDHSessionHeadersCache getInstance(Context context) {

        if(sharedInstance == null) {
            sharedInstance = new PDHSessionHeadersCache(context);
        }

        return sharedInstance;
    }

    public void savePDPJson(String accountNumber, String pdpjson) {

        ContentValues values = new ContentValues();
        values.put("pdpaddtocartjson",pdpjson);
        dbHelper.updateRow(sessioncache,values,"accountnumber='"+accountNumber+"'", null);
    }

    public HashMap<String, String> getSessionCookies(String accountnumber) {

        List<HashMap<String, String>> resultSet = dbHelper.executeSelect("select * from sessioncache where accountnumber='"+accountnumber+"'", null);

        if(resultSet == null || resultSet.size() == 0) {
            return null;
        }

        HashMap<String,String> row = resultSet.get(0);

        long time = Long.parseLong(row.get("lastrequested"));

        long currentTime = System.currentTimeMillis();

        if((currentTime-time) > PDHCacheDB.CACHE_TTL_MILLISECS) {
            deleteCookiesEntries(accountnumber);
            return null;
        }
        return row;
    }

    private void deleteCookiesEntries(String accountnumber) {

        dbHelper.executeDelete("delete from sessioncache where accountnumber='"+accountnumber+"'");

        return;
    }

    public boolean cacheCookies(String accountnumber , String jsidvzw, String jsessionid, String globalid) {

        ContentValues values = new ContentValues();
        values.put("accountnumber",accountnumber);
        values.put("jsidvzw",jsidvzw);
        values.put("jsessionid",jsessionid);
        values.put("globalid",globalid);
        values.put("lastrequested", System.currentTimeMillis());

        return dbHelper.insertRow(sessioncache, values);
    }
}
