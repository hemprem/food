/*
 *   -------------------------------------------------------------------------
 *      PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *      Wireless, Inc. and its affiliates except under written
 *      agreement.
 *
 *      This is an unpublished, proprietary work of Verizon Wireless,
 *      Inc. that is protected by United States copyright laws.  Disclosure,
 *      copying, reproduction, merger, translation,modification,enhancement,
 *      or use by anyone other than authorized employees or licensees of
 *      Verizon Wireless, Inc. without the prior written consent of
 *      Verizon Wireless, Inc. is prohibited.
 *
 *      Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *   -------------------------------------------------------------------------
 */

package com.food.core.cache;

import android.content.ContentValues;
import android.content.Context;

import java.util.HashMap;
import java.util.List;

/**
 * Created by bonamsa on 2/7/16.
 */

public class PDHMDNCache {

    public static String mdncache = "mdncache";

    private static PDHCacheDB dbHelper = null;

    private static PDHMDNCache sharedInstance;

    private PDHMDNCache(Context context) {

        if(dbHelper == null) {
            dbHelper = PDHCacheDB.getInstance(context);
            dbHelper.onCreate(dbHelper.getWritableDatabase());
        }
    }

    public static PDHMDNCache getInstance(Context context) {

        if(sharedInstance == null) {
            sharedInstance = new PDHMDNCache(context);
        }

        return sharedInstance;
    }

    public HashMap<String, String> getMDNDetails(String accountNumber) {

        List<HashMap<String, String>> resultSet = dbHelper.executeSelect("select * from mdncache where accountnumber='"+accountNumber+"'", null);

        if(resultSet == null || resultSet.size() == 0) {
            return null;
        }

        HashMap<String,String> row = resultSet.get(0);

        long time = Long.parseLong(row.get("lastrequested"));

        long currentTime = System.currentTimeMillis();

        if((currentTime-time) > PDHCacheDB.CACHE_TTL_MILLISECS) {
            deleteMTNDetails(accountNumber);
            return null;
        }
        return row;
    }

    private int deleteMTNDetails(String accountNumber) {

        dbHelper.executeDelete("delete from mdncache where accountnumber='"+accountNumber+"'");

        return 1;
    }

    public boolean cacheMtndetail(String accountNumber, String jsonResponse) {

        ContentValues values = new ContentValues();
        values.put("accountnumber",accountNumber);
        values.put("jsonresponse",jsonResponse);
        values.put("lastrequested", System.currentTimeMillis());

        return dbHelper.insertRow(mdncache, values);
    }
}
