/*
 *   -------------------------------------------------------------------------
 *      PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *      Wireless, Inc. and its affiliates except under written
 *      agreement.
 *
 *      This is an unpublished, proprietary work of Verizon Wireless,
 *      Inc. that is protected by United States copyright laws.  Disclosure,
 *      copying, reproduction, merger, translation,modification,enhancement,
 *      or use by anyone other than authorized employees or licensees of
 *      Verizon Wireless, Inc. without the prior written consent of
 *      Verizon Wireless, Inc. is prohibited.
 *
 *      Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *   -------------------------------------------------------------------------
 */

package com.food.core.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;
import com.food.core.utils.PDHConstants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bonamsa on 2/7/16.
 */
public class PDHCacheDB extends SQLiteOpenHelper {

    public static int CACHE_TTL_MILLISECS = 15*60*1000;

    public boolean insertRow (String tableName, ContentValues contentValues)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        long retVal = db.insert(tableName, null, contentValues);
        Log.d(PDHConstants.AppTag, "Insert Row Ret Value " + retVal);

        return true;
    }

    public int updateRow(String tableName, ContentValues values, String whereClause, String[] selectionArgs) {

        SQLiteDatabase db = this.getWritableDatabase();
        return db.update(tableName, values, whereClause, selectionArgs);
    }

    public ArrayList<HashMap<String, String>> executeSelect(String query, String[] selectionArg){

        return null;
        /*
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery(query, selectionArg);

        if(res.getCount() == 0) {
            return null;
        }

        ArrayList list = new ArrayList();

        res.moveToFirst();

        while(!res.isAfterLast()) {

            HashMap<String, String> row = new HashMap<String, String>();

            int noOfCols = res.getColumnCount();
            String colName = null;
            for (int i = 0; i < noOfCols; i++) {
                row.put(res.getColumnName(i), res.getString(i));
            }

            long time = Long.parseLong(row.get("lastrequested"));

            long currentTime = System.currentTimeMillis();

            res.moveToNext();
            list.add(row);
        }

        return list;
        */
    }

    public void executeDelete(String deleteQuery){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(deleteQuery);
        return ;
    }

    /*
     *
     * SQLHelper Lifecycle methods
     *
     */

    public Gson gson = new Gson();
    public static final String DATABASE_NAME = "vzweup.db";
    private static PDHCacheDB cacheDb = null;
    public static PDHCacheDB getInstance(Context context) {
        if(cacheDb == null) {
            cacheDb = new PDHCacheDB(context);
        }
        return cacheDb;
    }

    private PDHCacheDB(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        Log.d(PDHConstants.AppTag,"DB Exec SQL");

        db.execSQL("create table if not exists mdncache (id integer PRIMARY KEY autoincrement, accountnumber varchar(255), jsonresponse varchar(255),lastrequested varchar(20))");
        db.execSQL("create table if not exists pdpcache (id integer PRIMARY KEY autoincrement, deviceprodid varchar(20), deviceskuid varchar(20), selectedmtn varchar(20), jsonresponse varchar(255), lastrequested varchar(20))");
        db.execSQL("create table if not exists gwcache (id integer PRIMARY KEY autoincrement, selectionquerystring varchar(100), selectedmtn varchar(20), showall varchar(6),jsonresponse varchar(255) , lastrequested varchar(20))");
        db.execSQL("create table if not exists sessioncache (id integer PRIMARY KEY autoincrement, accountnumber varchar(100), jsidvzw varchar(20), jsessionid varchar(6),globalid varchar(255), pdpaddtocartjson varchar(200), lastrequested varchar(20))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        Log.d(PDHConstants.AppTag, "DB Upgrade");
        onCreate(db);
    }
}
