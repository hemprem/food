package com.food.core.session;

import java.io.Serializable;

/**
 * Created by bonamsa on 2/13/16.
 */
public class PDHSession implements Serializable {

    //Cookie Values
    public String jSIDVZWValue = null;
    public String jSessionIdValue = null;
    public String globalIdValue = null;

    //Profile Information
    public String accountNumber = null;
    public String loginMtn = null;
    public String selectedMtn = null;

    public int lastLoginTimeInSecs;

    public String cartOrderId = null;
    public String cartCommerceItemId = null;
}
