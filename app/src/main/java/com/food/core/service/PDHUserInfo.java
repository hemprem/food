/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.service;

import com.food.core.utils.PDHAppState;

/**
 * Created by shaimu8 on 1/15/16.
 */
public class PDHUserInfo {
    private String accountNumber;
    private String loginMTN;
    public Boolean edgeUpBuyOutAccepted;

    public PDHUserInfo() {
        this.accountNumber = PDHAppState.getInstance().loginAcNumber;
        this.loginMTN = PDHAppState.getInstance().loginMtnNumber;
        this.edgeUpBuyOutAccepted = null;
    }

    public PDHUserInfo(String loginAcNumber, String loginMTN) {
        this.accountNumber = loginAcNumber;
        this.loginMTN = loginMTN;
        this.edgeUpBuyOutAccepted = null;
    }

    public void setAccountNumber(String value) { accountNumber = value; }
    public String getAccountNumber() { return accountNumber; }
    public void setLoginMTN(String value) { loginMTN = value; }
    public String getLoginMTN() { return loginMTN; }
}
