/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.service;

import android.content.Context;

import com.food.core.common.PDHHTTPClientFactory;
import com.food.core.utils.PDHAppState;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.food.core.ShopKitApp;

import java.io.InputStream;

import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Vikas Hasija on 12/24/15.
 */
public class PDHGenericServicesHandler {

    public String baseUrl;
    public Context context;

    public PDHGenericServicesHandler() {
        context = ShopKitApp.getInstance().getAppContext();
        setBaseUrl();
    }

    public void setBaseUrl() {
        baseUrl = PDHAppState.getInstance().hostURL;
    }

    protected Retrofit getRetrofit() {

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);
        OkHttpClient client = PDHHTTPClientFactory.getVZWHTTPClientAddCookiesToHeader(false, true);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(converterFactory)
                .build();
        return retrofit;
    }

    protected class Callback implements retrofit.Callback {

        private PDHAPIProviderCallback VZWAPIProviderCallback;
        public Callback(PDHAPIProviderCallback VZWAPIProviderCallback) {
            this.VZWAPIProviderCallback = VZWAPIProviderCallback;
        }

        @Override
        public void onResponse(retrofit.Response response, Retrofit retrofit) {
            PDHAPIDataModelBase body = (PDHAPIDataModelBase) response.body();
            VZWAPIProviderCallback.onResponse(null, body);
        }

        @Override
        public void onFailure(Throwable t) {
            VZWAPIProviderCallback.onResponse(t.toString(), null);
        }

        public void loadDataFromCache(String path, Class cls) {
            try {
                InputStream is = context.getAssets().open(path);
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                String bufferString = new String(buffer);
                Gson gson = new Gson();
                PDHAPIDataModelBase model = (PDHAPIDataModelBase) gson.fromJson(bufferString, cls);
                VZWAPIProviderCallback.onResponse(null, model);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
