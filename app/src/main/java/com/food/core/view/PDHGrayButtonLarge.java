/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.food.R;

public class PDHGrayButtonLarge extends PDHButton {
    public PDHGrayButtonLarge(Context context) { this(context, null); }
    public PDHGrayButtonLarge(Context context, AttributeSet attrs) { this(context, attrs, 0); }
    public PDHGrayButtonLarge(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        int[] tArrAttrs = new int[]{ android.R.attr.textColor,
                                     android.R.attr.background};
        TypedArray tArr = context.obtainStyledAttributes(attrs, tArrAttrs);

        backgroundColor = tArr.getColor(1, ContextCompat.getColor(context, R.color.pdh_default_grey_text));
        backgroundColorDepressed = ContextCompat.getColor(context, R.color.secondary_button_depressed);
        textColor = tArr.getColor(0, ContextCompat.getColor(context, R.color.secondary_button_text));

        setTextColor(textColor);
        setBackgroundColor(backgroundColor);

        tArr.recycle();
    }
}
