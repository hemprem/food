/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.view;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.widget.Button;
import com.food.R;
import com.food.core.utils.PDHFontUtils;

public class PDHButton extends Button {
    protected int backgroundColor = 0;
    protected int backgroundColorDepressed = 0;
    protected int backgroundColorDisabled = 0;
    protected int textColor = 0;
    protected int textColorDisabled = 0;
    protected boolean disabled = false;
    protected ValueAnimator timeAnimator = ValueAnimator.ofFloat(0f, 1f);
    protected ArgbEvaluator rgbEval = new ArgbEvaluator();
    protected long animationDuration = 150;
    protected boolean depressed = false;
    public static final float DEFAULT_TEXTSIZE = 16f;
    public static final float DEFAULT_PADDING_SIZE = 16f;

    public PDHButton(Context context) { this(context, null); }
    public PDHButton(Context context, AttributeSet attrs) { this(context, attrs, 0); }
    public PDHButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        float density = getResources().getDisplayMetrics().density;

        int[] tArrAttrs = new int[]{ android.R.attr.textSize,
                                     android.R.attr.paddingLeft,
                                     android.R.attr.paddingTop,
                                     android.R.attr.paddingRight,
                                     android.R.attr.paddingBottom,
                                     android.R.attr.textColor,
                                     android.R.attr.background};
        TypedArray tArr = context.obtainStyledAttributes(attrs, tArrAttrs);

        backgroundColor = tArr.getColor(6, ContextCompat.getColor(context, R.color.red_button));
        backgroundColorDepressed = ContextCompat.getColor(context, R.color.red_button_depressed);
        textColor = tArr.getColor(5, ContextCompat.getColor(context, R.color.red_button_text));
        backgroundColorDisabled = ContextCompat.getColor(context, R.color.red_button_disabled);
        textColorDisabled = ContextCompat.getColor(context, R.color.red_button_text_disabled);

        setTypeface(PDHFontUtils.getInstance().getFont(PDHFontUtils.FONT_NHG_DIPSLAYBOLD));
        setTextSize(tArr.getDimension(0, (int) DEFAULT_TEXTSIZE));
        setPadding(tArr.getDimensionPixelSize(1, (int) (density * DEFAULT_PADDING_SIZE)),
                tArr.getDimensionPixelSize(2, (int) (density * DEFAULT_PADDING_SIZE)),
                tArr.getDimensionPixelSize(3, (int) (density * DEFAULT_PADDING_SIZE)),
                tArr.getDimensionPixelSize(4, (int) (density * DEFAULT_PADDING_SIZE)));
        setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
        setTextColor(textColor);
        setBackgroundColor(backgroundColor);

        timeAnimator.setDuration(animationDuration);
        timeAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                float progress = (float) animator.getAnimatedValue();
                if(isEnabled()) {
                    if (depressed) {
                        setBackgroundColor( (int) rgbEval.evaluate(progress, backgroundColor, backgroundColorDepressed) );
                    } else {
                        setBackgroundColor( (int) rgbEval.evaluate(progress, backgroundColorDepressed, backgroundColor) );
                    }
                    invalidate();
                }
            }
        });

        tArr.recycle();
    }

    @Override
    public void setEnabled(boolean enabled) {
        boolean updateColors = false;
        if(enabled != isEnabled()) {
            updateColors = true;
        }
        super.setEnabled(enabled);
        if(updateColors && enabled==isEnabled()) {
            timeAnimator.cancel();
            if(isEnabled()) {
                setBackgroundColor(backgroundColor);
                setTextColor(textColor);
            } else {
                setBackgroundColor(backgroundColorDisabled);
                setTextColor(textColorDisabled);
            }
        }
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(isEnabled()) {
        if(event.getAction()==MotionEvent.ACTION_DOWN) {
                depressed = true;
                animateBG();
        } else if(event.getAction()==MotionEvent.ACTION_UP || (event.getAction()==MotionEvent.ACTION_CANCEL && depressed)) {
                depressed = false;
                animateBG();
            }
        }

        super.onTouchEvent(event);

        return true;
    }

    private void animateBG() {
        timeAnimator.cancel();
        timeAnimator.start();
    }
}
