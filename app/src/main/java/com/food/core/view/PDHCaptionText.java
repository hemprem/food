/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.food.R;
import com.food.core.utils.PDHFontUtils;


public class PDHCaptionText extends TextView {
    private static float DEFAULT_FONTSIZE = 12f;
    public PDHCaptionText(Context context) { this(context, null); }
    public PDHCaptionText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }
    public PDHCaptionText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        int[] tArrAttrs = new int[]{ android.R.attr.textSize, android.R.attr.textColor };
        TypedArray tArr = context.obtainStyledAttributes(attrs, tArrAttrs);

        setTypeface(PDHFontUtils.getInstance().getFont(PDHFontUtils.FONT_NHG_TEXT));
        int size = tArr.getDimensionPixelSize(0, -1);
        if(size==-1) {
            setTextSize(TypedValue.COMPLEX_UNIT_SP, DEFAULT_FONTSIZE);
        } else {
            setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
        setTextColor(tArr.getColor(1, ContextCompat.getColor(context, R.color.textCaption)));

        tArr.recycle();
    }
}
