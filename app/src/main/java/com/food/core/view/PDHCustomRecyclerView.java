/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;

public class PDHCustomRecyclerView extends RecyclerView {
    public PDHCustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PDHCustomRecyclerView(Context context) {
        super(context);
    }

    public PDHCustomRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int expandSpec = MeasureSpec.makeMeasureSpec(0,
                MeasureSpec.EXACTLY);
        Log.d("VZWCustomRecyclerView", "expandSpec<2> = "+expandSpec);
        //Log.d("VZWCustomRecyclerView", "onMeasure widthMeasureSpec = "+widthMeasureSpec+", heightMeasureSpec ="+heightMeasureSpec);
        //Log.d("VZWCustomRecyclerView", "onMeasure expandSpec = " + expandSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
