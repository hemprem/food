/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.food.core.utils.PDHFontUtils;

public class PDHIcon extends TextView {
    private static final float DEFAULT_TEXTSIZE = 12f;

    public PDHIcon(Context context) { this(context, null); }
    public PDHIcon(Context context, AttributeSet attrs) { this(context, attrs, 0); }
    public PDHIcon(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

//        float density = getResources().getDisplayMetrics().density;

        int[] tArrAttrs = new int[]{ android.R.attr.textSize };
        TypedArray tArr = context.obtainStyledAttributes(attrs, tArrAttrs);

        setTypeface(PDHFontUtils.getInstance().getFont(PDHFontUtils.FONT_VZWICON));
        int size = tArr.getDimensionPixelSize(0, -1);
        if(size==-1) {
            setTextSize(TypedValue.COMPLEX_UNIT_SP, DEFAULT_TEXTSIZE);
        } else {
            setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }

        tArr.recycle();
    }
}
