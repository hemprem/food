/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.common.http;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by bonamsa on 12/30/15.
 */
public class PDHNetworkCacheInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());
        //Cache Time To Live
        int timeToLive = 8*60*60;
        return originalResponse.newBuilder()
                .header("Cache-Control", String.format("max-age=%d, only-if-cached", timeToLive))
                .build();
    }
}
