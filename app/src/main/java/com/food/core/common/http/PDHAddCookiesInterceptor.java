/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.common.http;

import android.util.Log;

import com.food.core.utils.PDHAppState;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by bonamsa on 12/30/15.
 */
public class PDHAddCookiesInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request.Builder builder = chain.request().newBuilder();

       if(PDHAppState.jsidVzwCookie != null) {
            builder.addHeader("Cookie", PDHAppState.jsidVzwCookie.getName()+"="+ PDHAppState.jsidVzwCookie.getValue());
            Log.i("Cookie", "Added jsidVzw: "+ PDHAppState.jsidVzwCookie.getName()+"="+ PDHAppState.jsidVzwCookie.getValue());
        }

        if(PDHAppState.jsessionIdCookie!= null) {
            builder.addHeader("Cookie", PDHAppState.jsessionIdCookie.getName()+"="+ PDHAppState.jsessionIdCookie.getValue());
            Log.i("Cookie", "Added jsessionId: " + PDHAppState.jsessionIdCookie.getName() + "=" + PDHAppState.jsessionIdCookie.getValue());
        }

        if(PDHAppState.globalIdCookie!= null) {
            builder.addHeader("Cookie", PDHAppState.globalIdCookie.getName()+"="+ PDHAppState.globalIdCookie.getValue());
            Log.i("Cookie", "Added globalId: " + PDHAppState.globalIdCookie.getName() + "=" + PDHAppState.globalIdCookie.getValue());
        }

        return chain.proceed(builder.build());
    }



}