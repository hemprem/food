/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.common.http;

import android.util.Log;

import com.food.core.utils.PDHAppState;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Response;
import com.food.core.cache.PDHSessionHeadersCache;

import java.io.IOException;
import java.net.HttpCookie;

/**
 * Created by bonamsa on 12/30/15.
 */
public class PDHReceivedCookiesInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {

            for (String header : originalResponse.headers("Set-Cookie")) {
                Log.i("Cookie", "Header: " + header);
                try {

                    HttpCookie cookie = this.parseRawCookie(header);

                    if(cookie.getName().equals(PDHAppState.jSessionIdKey)) {
                        PDHAppState.jsessionIdCookie = cookie;
                        Log.i("Cookie", "Added jsessionIdCookie: " + PDHAppState.jsessionIdCookie.getName() + "=" + PDHAppState.jsessionIdCookie.getValue());

                    } else if(cookie.getName().equals(PDHAppState.jSIDVZWKey)) {
                        PDHAppState.jsidVzwCookie = cookie;
                        Log.i("Cookie", "Added jsidVzwCookie: " + PDHAppState.jsidVzwCookie.getName() + "=" + PDHAppState.jsidVzwCookie.getValue());

                    } else if(cookie.getName().equals(PDHAppState.globalIdKey)) {
                        PDHAppState.globalIdCookie = cookie;
                        Log.i("Cookie", "Added globalIdCookie: " + PDHAppState.globalIdCookie.getName() + "=" + PDHAppState.globalIdCookie.getValue());
                    }

                    String loginAccNumber = PDHAppState.getInstance().loginAcNumber;
                    PDHSessionHeadersCache.getInstance(PDHAppState.getInstance().applicationContext).cacheCookies(loginAccNumber, PDHAppState.jsidVzwCookie.getValue(), PDHAppState.jsessionIdCookie.getValue(), PDHAppState.globalIdCookie.getValue());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return originalResponse;
    }

    public HttpCookie parseRawCookie(String rawCookie) throws Exception {

        String[] rawCookieParams = rawCookie.split(";");

        String[] rawCookieNameAndValue = rawCookieParams[0].split("=");
        if (rawCookieNameAndValue.length != 2) {
            throw new Exception("Invalid cookie: missing name and value.");
        }

        String cookieName = rawCookieNameAndValue[0].trim();
        String cookieValue = rawCookieNameAndValue[1].trim();
        HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
        for (int i = 1; i < rawCookieParams.length; i++) {
            String rawCookieParamNameAndValue[] = rawCookieParams[i].trim().split("=");

            String paramName = rawCookieParamNameAndValue[0].trim();

            if (paramName.equalsIgnoreCase("secure")) {
                cookie.setSecure(true);
            } else {
                if (rawCookieParamNameAndValue.length != 2) {
                    throw new Exception("Invalid cookie: attribute not a flag or missing value.");
                }

                String paramValue = rawCookieParamNameAndValue[1].trim();
            }
        }

        return cookie;
    }
}

