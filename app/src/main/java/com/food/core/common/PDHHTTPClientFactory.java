/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.common;

import android.util.Log;

import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHConstants;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import com.food.core.common.http.PDHAddCookiesInterceptor;
import com.food.core.common.http.PDHAuthHeadersInterceptor;
import com.food.core.common.http.PDHNetworkCacheInterceptor;
import com.food.core.common.http.PDHReceivedCookiesInterceptor;

import java.io.File;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

/**
 * Created by bonamsa on 12/28/15.
 */
public final class PDHHTTPClientFactory {

    private static String fileLocation = null;

    private static Cache cache;

    private final static long MAX_CACHE_SIZE = 1024 * 1024;//1MB

    private static OkHttpClient createHttpClient(boolean secureEnabled) {

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(300, TimeUnit.SECONDS);
        client.setReadTimeout(300, TimeUnit.SECONDS);
        client.setFollowSslRedirects(true);
        client.setFollowRedirects(true);
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_NONE);
        client.setCookieHandler(cookieManager);

        client.interceptors().add(new PDHAuthHeadersInterceptor(secureEnabled));

        if(PDHAppState.getInstance().isToLogServerCalls) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            // set your desired log level
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            client.interceptors().add(loggingInterceptor);
        }

        return client;
    }

    private static File getDirectory() {

        File folder = new File(PDHAppState.getInstance().applicationContext.getFilesDir() + "/shopkit");
        Log.d(PDHConstants.AppTag,"Cache Location\n"+folder.getAbsolutePath());

        if(!folder.exists()) {
            folder.mkdir();

            Log.d(PDHConstants.AppTag,"Cache Directory Created");
        }

        return folder;
    }

    public static OkHttpClient getVZWHTTPClient(boolean secureEnabled) {
        return PDHHTTPClientFactory.createHttpClient(secureEnabled);
    }

    public static OkHttpClient getVZWHTTPClientAddCookiesToHeader(boolean secureEnabled) {
        return PDHHTTPClientFactory.getVZWHTTPClientAddCookiesToHeader(false, secureEnabled);
    }

    public static OkHttpClient getVZWHTTPClientAddCookiesToHeader(boolean cacheEnabled, boolean secureEnabled) {

        OkHttpClient client = PDHHTTPClientFactory.createHttpClient(secureEnabled);
        client.interceptors().add(new PDHAddCookiesInterceptor());

        if(PDHAppState.getInstance().isToLogServerCalls) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            // set your desired log level
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            client.interceptors().add(loggingInterceptor);
        }

        if(cacheEnabled) {
            client.networkInterceptors().add(new PDHNetworkCacheInterceptor());
            client.setCache(getCache());
        }

        return client;
    }

    private static Cache getCache() {
        if(cache == null) {
            cache = new Cache(getDirectory(), MAX_CACHE_SIZE);
        }

        return cache;
    }

    public static OkHttpClient getVZWHTTPClientReadCookiesFromHeader(boolean secureEnabled) {
        return PDHHTTPClientFactory.getVZWHTTPClientReadCookiesFromHeader(false, secureEnabled);
    }

    public static OkHttpClient getVZWHTTPClientReadCookiesFromHeader(boolean cacheEnabled, boolean secureEnabled) {

        OkHttpClient client = PDHHTTPClientFactory.createHttpClient(secureEnabled);
        client.interceptors().add(new PDHReceivedCookiesInterceptor());

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        // set your desired log level
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(loggingInterceptor);

        if(cacheEnabled) {
            client.networkInterceptors().add(new PDHNetworkCacheInterceptor());
            client.setCache(getCache());
        }

        return client;
    }
}