package com.food.core.utils;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AesUtilHelper {
    private final int keySize;
    private final int iterationCount;
    private final Cipher cipher;
    
    public AesUtilHelper(int keySize, int iterationCount) {
        this.keySize = keySize;
        this.iterationCount = iterationCount;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        }
        catch (Exception e) {
            throw fail(e);
        }
    }
    
    public String encrypt(String salt, String iv, String passphrase, String plaintext) {
        try {
            SecretKey key = generateKey(salt, passphrase);
            byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, iv, plaintext.getBytes("UTF-8"));
            return base64(encrypted);
        }
        catch (UnsupportedEncodingException e) {
            throw fail(e);
        }
    }
    
    public String decrypt(String salt, String iv, String passphrase, String ciphertext) {
        try {
            SecretKey key = generateKey(salt, passphrase);
            //System.out.println("Helper decrypt="+key+"="+salt+"="+passphrase+"="+iv+"="+ciphertext);
            byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, iv, base64(ciphertext));
            //System.out.println(decrypted);
            String out = new String(decrypted, "UTF-8");
            //System.out.println(out);
            return out;
        }
        catch (UnsupportedEncodingException e) {
            throw fail(e);
        }
    }
    
    private byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[] bytes) {
        try {
            cipher.init(encryptMode, key, new IvParameterSpec(hex(iv)));
            return cipher.doFinal(bytes);
        }
        catch (Exception e) {
        	System.out.println("AesUtilHelper.doFinal() ::>"+e.getMessage());
            throw fail(e);
        }
    }
    
    private SecretKey generateKey(String salt, String passphrase) {
    	if(null == passphrase || null == salt){
    		return null;
    	}
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), hex(salt), iterationCount, keySize);
            SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
            return key;
        }
        catch (Exception e) {
        	System.out.println("AesUtilHelper.generateKey() ::>"+e.getMessage());
           return null;
        }
    }
    
    public static String random(int length) {
        byte[] salt = new byte[length];
        new SecureRandom().nextBytes(salt);
        return hex(salt);
    }
    
    public static String base64(byte[] bytes) {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }
    
    public static byte[] base64(String str) {
        return Base64.decode(str, Base64.DEFAULT);
    }


    public static String hex(byte[] bytes) {
        return toHexString(bytes);
    }

    public static byte[] hex(String str) {
        return bytesFromHex(str);
    }

    public static String toHexString(byte[] bytes) {
        char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;
            hexChars[j*2] = hexArray[v/16];
            hexChars[j*2 + 1] = hexArray[v%16];
        }
        return new String(hexChars);
    }


    public static byte[] bytesFromHex(String hexString) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = hexString.toCharArray();
        byte[] result = new byte[hexChars.length / 2];
        for (int j = 0; j < hexChars.length; j += 2) {
            result[j / 2] = (byte) (Arrays.binarySearch(hexArray, hexChars[j]) * 16 + Arrays.binarySearch(hexArray, hexChars[j + 1]));
        }
        return result;
    }

    private IllegalStateException fail(Exception e) {
        return new IllegalStateException(e);
    }
}
