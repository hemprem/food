/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.HashMap;

/**
 * Created by shaimu8 on 12/24/15.
 */
public class PDHAppState {

    private static PDHAppState instance;

    /**
     * Cache Variables
     */

    private JSONObject pdpSuccessStates;

    /**
     * AppConfigs
     */
    public static Boolean CLASSIC_DESIGN = true;
    public static Boolean DOUG_DESIGN = true;

    public static Boolean EDGE_BUY_OUT_ACCEPTED = false;


    //ORDER Related Fields
    public static String pdpAction = null;
//    public static String placedOrderId;
//    public static String commerceItemId;
//    public static int contractTerm;

   // public static VZWPDPAddOrUpdateDeviceResponseModel pdpAddToCartModel;
   //public static VZWPDPAddOrUpdateDeviceRequest pdpAddToCartRequest;
    public static Double edgeBuyoutAmount = 0.0;

    public Context applicationContext;

   // public static VZWPDPDialogFragment pdpFragment = null;

    public String loginAcNumber;
    public String loginMtnNumber;
    public String selectedMtn = null;
    //public String selectedOSName;
    //public boolean isAddTOCartActive = false;
    public static String selectedContractTerm = "99";
    public boolean loadFromServer = true;
    public Boolean encodeInputJSON = true;
    private String tradeInAppraiseId = "";
    private String tradeInValue = "";
    public String deviceProtectionSfoSkuID;
    public Boolean tradeInOpted = null;

    public String hostURL;

    public static String jSIDVZWKey = "JSIDVZW";
    public static String jSessionIdKey = "JSESSIONID";
    public static String globalIdKey = "GLOBALID";

    public static HttpCookie jsidVzwCookie;
    public static HttpCookie jsessionIdCookie;
    public static HttpCookie globalIdCookie;

    public Boolean isToLogServerCalls = true;

    public static Boolean isDisplayTradeInDialog = true;

    public HashMap<String, String> selectedPDP;

    static public PDHAppState getInstance() {
        if (instance == null) {
            instance = new PDHAppState();
        }
        return instance;
    }

    public PDHAppState() {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_NONE);
        hostURL = "https://ecommservicespp.verizonwireless.com";
//        hostURL = "https://ecommservicespp.verizonwireless.com";
//        hostURL = "http://10.20.50.168:8000";
//        hostURL = "https://vzwqa6.verizonwireless.com";
//      hostURL = "http://wca10653lditbbi.uswin.ad.vzwcorp.com:8000"
    }

    public static void doNullPDPData() {
        PDHAppState.getInstance().deviceProtectionSfoSkuID = null;
       // PDHAppState.pdpAddToCartModel = null;
       // PDHAppState.pdpAddToCartRequest = null;
        PDHAppState.pdpAction = null;
        PDHAppState.isDisplayTradeInDialog = true;
        PDHAppState.getInstance().setTradeInAppraiseId(null);
        PDHAppState.getInstance().setTradeInValue(null);
    }

/*    public boolean isAddTOCartActive() {
        return isAddTOCartActive;
    }

    public void setIsAddTOCartActive(boolean isAddTOCartActive) {
        this.isAddTOCartActive = isAddTOCartActive;
    }*/

    public String getTradeInAppraiseId() {
        return tradeInAppraiseId;
    }

    public void setTradeInAppraiseId(String tradeInAppraiseId) {
        this.tradeInAppraiseId = tradeInAppraiseId;
    }

    public String getTradeInValue() {
        return tradeInValue;
    }

    public void setTradeInValue(String tradeInValue) {
        this.tradeInValue = tradeInValue;
    }

    private JSONObject getPDPResponseStates() {

        if(pdpSuccessStates == null) {

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
            try {
                pdpSuccessStates = new JSONObject(sharedPreferences.getString("pdpSuccessStates", "{}"));
            } catch (JSONException e) {
                e.printStackTrace();
                pdpSuccessStates = new JSONObject();
            }
        }

        return pdpSuccessStates;
    }

    public boolean getPDPResponseState(String productId) {

        boolean trueState = false;

        try {
            trueState = getPDPResponseStates().getBoolean(productId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return trueState;
    }

    public void setPDPResponseState(String productId, boolean responseState) {

        if(!responseState) {
            return;
        }
        try {
            //Update pdpResponseState JSONObject
            getPDPResponseStates().put(productId,responseState);
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit();
            //Update pdpResponseState to shared preferences.
            editor.putString("pdpSuccessStates", getPDPResponseStates().toString());
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
