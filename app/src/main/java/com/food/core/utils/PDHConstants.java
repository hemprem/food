/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.utils;

/**
 * Created by bonamsa on 12/2/15.
 */
public class PDHConstants {

    public static final String AppTag = "MVM";
    public static final String Metrics = "METRICS";

    public static final String GridwallModeFlagKey = "GridwallModeFlagKey";
    public static final String TECH_COACH_TERMS_CONDITION_URL = "http://www.verizonwireless.com/support/tech-coach-legal/";
    public static final String TECH_COACH_TITLE = "Terms & Conditions - Tech Coach";
    public static final String TECH_COACH_BUTTON_TEXT = "OK";
    public static final String DEVICE_PROTECTION_TOTAL_MOBILE_PROTECTION_URL = "http://www.verizonwireless.com/support/total-equipment-coverage-legal/";
    public static final String DEVICE_PROTECTION_TOTAL_MOBILE_PROTECTION_TITLE = "Terms & Conditions - Total Mobile Protection Coverage";
    public static final String DEVICE_PROTECTION_TOTAL_MOBILE_PROTECTION_BUTTON_TEXT = "OK";
    public static final String DEVICE_PROTECTION_TOTAL_EQUIPMENT_COVERAGE_URL = "http://www.verizonwireless.com/support/equipment-protection-legal/";
    public static final String DEVICE_PROTECTION_TOTAL_EQUIPMENT_COVERAGE_TITLE = "Terms & Conditions - Equipment Protection";
    public static final String DEVICE_PROTECTION_TOTAL_EQUIPMENT_COVERAGE_BUTTON_TEXT = "OK";
    public static final String DEVICE_PROTECTION_WIRELESS_PHONE_PROTECTION_URL = "http://www.verizonwireless.com/support/wireless-phone-protection-legal/";
    public static final String DEVICE_PROTECTION_WIRELESS_PHONE_PROTECTION_TITLE = "Wireless Phone Protection - Terms & Conditions";
    public static final String DEVICE_PROTECTION_WIRELESS_PHONE_PROTECTION_BUTTON_TEXT = "OK";
    public static final String DEVICE_PROTECTION_EXTENDED_WARRANTY_URL = "http://www.verizonwireless.com/support/extended-warranty-legal/";
    public static final String DEVICE_PROTECTION_EXTENDED_WARRANTY_TITLE = "Terms & Conditions - Extended Limited Warranty or Service Contract";
    public static final String DEVICE_PROTECTION_EXTENDED_WARRANTY_BUTTON_TEXT = "OK";
    public static final String DEVICE_PROTECTION_EQUIPMENT_PROTECTION_PROGRAMS_URL = "http://www.verizonwireless.com/support/equipment-protection-overview/";
    public static final String DEVICE_PROTECTION_EQUIPMENT_PROTECTION_PROGRAMS_TITLE = "Equipment Protection Programs";
    public static final String DEVICE_PROTECTION_EQUIPMENT_PROTECTION_PROGRAMS_BUTTON_TEXT = "OK";
    public static final String Scene7URL = "http://s7.vzw.com/is/image/VerizonWireless/";
    public static final String SERVER_SUCCESS_CODE = "0";
}
