/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.utils;

import android.content.res.AssetManager;
import android.graphics.Typeface;

import java.util.HashMap;

public class PDHFontUtils {
    //Private Vars
    private static PDHFontUtils instance = null;
    private HashMap<Short, String> assets = null;
    private HashMap<Short, Typeface> fonts = null;
    private AssetManager assetManager;
    //Public Vars
    public static final short FONT_NHG_DIPSLAY = 0;
    public static final short FONT_NHG_DIPSLAYBOLD = 1;
    public static final short FONT_NHG_DIPSLAYMEDIUM = 2;
    public static final short FONT_NHG_TEXT = 3;
    public static final short FONT_NHG_TEXTBOLD = 4;
    public static final short FONT_NHG_TEXTMEDIUM = 5;
    public static final short FONT_VZWICON = 6;

    /*** Constructor ***/
    protected PDHFontUtils() {
        assets = new HashMap<Short, String>();
        fonts = new HashMap<Short, Typeface>();

        assets.put(FONT_NHG_DIPSLAY, "fonts/NeueHaasGroteskDisplay.ttf");
        assets.put(FONT_NHG_DIPSLAYBOLD, "fonts/NeueHaasGroteskDisplayBold.ttf");
        assets.put(FONT_NHG_DIPSLAYMEDIUM, "fonts/NeueHaasGroteskDisplayMedium.ttf");
        assets.put(FONT_NHG_TEXT, "fonts/NeueHaasGroteskText.ttf");
        assets.put(FONT_NHG_TEXTBOLD, "fonts/NeueHaasGroteskTextBold.ttf");
        assets.put(FONT_NHG_TEXTMEDIUM, "fonts/NeueHaasGroteskTextMedium.ttf");
        assets.put(FONT_VZWICON, "fonts/vzw-iconfont.ttf");
    }

    /*** Public Static ***/
    public static PDHFontUtils getInstance() {
        if(instance==null) { instance = new PDHFontUtils(); }
        return instance;
    }

    /*** GETTERS ***/
    public AssetManager getAssetManager() {
        return assetManager;
    }
    /*** SETTERS ***/
    public void setAssetManager(AssetManager pAssetManager) {
        assetManager = pAssetManager;
    }

    /*** Public ***/
    public Typeface getFont(Short fontID) {
        if(fontID!=null && assets.containsKey(fontID) && getAssetManager()!=null) {
            if(fonts.containsKey(fontID)) {
                return fonts.get(fontID);
            } else {
                Typeface font = Typeface.createFromAsset(getAssetManager(), assets.get(fontID));
                if(font!=null) {
                    fonts.put(fontID, font);
                    return font;
                }
            }
        }
        return null;
    }
}
