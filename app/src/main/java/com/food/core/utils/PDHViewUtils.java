/*
 *  -------------------------------------------------------------------------
 *     PROPRIETARY INFORMATION.  Not for use or disclosure outside Verizon
 *     Wireless, Inc. and its affiliates except under written
 *     agreement.
 *
 *     This is an unpublished, proprietary work of Verizon Wireless,
 *     Inc. that is protected by United States copyright laws.  Disclosure,
 *     copying, reproduction, merger, translation,modification,enhancement,
 *     or use by anyone other than authorized employees or licensees of
 *     Verizon Wireless, Inc. without the prior written consent of
 *     Verizon Wireless, Inc. is prohibited.
 *
 *     Copyright (c) 2016 Verizon Wireless, Inc.  All rights reserved.
 *  -------------------------------------------------------------------------
 */

package com.food.core.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.food.core.view.PDHCustomProgressDialog;

import java.util.concurrent.atomic.AtomicInteger;



/**
 * Created by shaimu8 on 12/24/15.
 */
public class PDHViewUtils {
    static PDHViewUtils instance;

    private Boolean isProgressShown;
    private PDHCustomProgressDialog customProgressDialog;
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    static public PDHViewUtils getInstance() {
        if (instance == null) {
            instance = new PDHViewUtils();
        }
        return instance;
    }

    public void showProgress(Context context) {
        if (isProgressShown != null && isProgressShown) {
            return;
        }
        isProgressShown = true;
        if(context instanceof FragmentActivity) {
            FragmentActivity activity = (FragmentActivity)context;
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            hideProgress();
            customProgressDialog = PDHCustomProgressDialog.newInstance("Loading ...");
            try {
                customProgressDialog.show(ft, "CustomDialog");
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("VZWViewUtil", "Show progress dialog exception: " + e.toString());
            }
        }
    }

    public void hideProgress() {
        isProgressShown = false;
        try {
            if (customProgressDialog != null) {
                customProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PDHViewUtils", "Dismiss progress dialog exception:: " + e.toString());
        }
    }

 /*   public static void showError(Context context, String modalTitle, String contentTitle, String contentMessage, String buttonText, Boolean showCloseIcon, final DialogInterface.OnClickListener clickListener) {
        final AlertDialog alert = new AlertDialog.Builder(context).setView(R.layout.alert_dialog).show();
        alert.setCanceledOnTouchOutside(false);
        TextView modalHeadingView = (TextView) alert.findViewById(R.id.modal_heading_title);
        TextView titleView = (TextView) alert.findViewById(R.id.title);
        TextView messageView = (TextView) alert.findViewById(R.id.message);
        VZWRedButtonLargeRightArrow btn = (VZWRedButtonLargeRightArrow) alert.findViewById(R.id.button);
        TextView closeIconView = (TextView)alert.findViewById(R.id.modal_close_icon);

        if(modalTitle!=null && modalTitle.length()>0) {
            modalHeadingView.setText(modalTitle);
        } else {
            alert.findViewById(R.id.modal_heading).setVisibility(View.GONE);
        }
        if(contentTitle!=null && contentTitle.length()>0) {
            titleView.setText(contentTitle);
        } else {
            titleView.setVisibility(View.GONE);
        }
        if(contentMessage!=null && contentMessage.length()>0) {
            messageView.setText(contentMessage);
        } else {
            messageView.setVisibility(View.GONE);
        }
        if(showCloseIcon!=null && showCloseIcon==true) {
            closeIconView.setVisibility(View.VISIBLE);
            closeIconView.setClickable(true);
            closeIconView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(clickListener !=null) {
                        clickListener.onClick(alert, 0);
                    }
                    if (alert != null) {
                        alert.dismiss();
                    }
                }
            });
        } else {
            closeIconView.setVisibility(View.GONE);
        }
        if(buttonText!=null && buttonText.length()>0) {
            btn.setText(buttonText);
        }
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(clickListener !=null) {
                    clickListener.onClick(alert, 0);
                }
                if (alert != null) {
                    alert.dismiss();
                }
            }
        });
    }*/

    public static void showError(Context context, String title, String message) {
     //   showError(context, title, null, message, null, null, null);
    }

    public static void showHintMessage(Context context, String message) {
        AlertDialog alert = new AlertDialog.Builder(context)
            .setMessage(message)
            .setNeutralButton("OK",  new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            })
            .setIcon(android.R.drawable.ic_dialog_info)
            .show();
        alert.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#cd040b"));
    }

    public static void showErrorDialog(Activity activity, String title, String message, String positiveButtonTitle, final DialogInterface.OnClickListener positiveListener, String negativeButtonTitle, final DialogInterface.OnClickListener negativeListener) {
       // showErrorDialog(activity, title, null, message, positiveButtonTitle, positiveListener, negativeButtonTitle, negativeListener);
    }

  /*  public static void showErrorDialog(Activity activity, String modalTitle, String contentTitle, String contentMessage, String positiveButtonTitle, final DialogInterface.OnClickListener positiveListener, String negativeButtonTitle, final DialogInterface.OnClickListener negativeListener) {
        final AlertDialog alert = new AlertDialog.Builder(activity).setView(R.layout.alert_dialog_choice).show();
        alert.setCanceledOnTouchOutside(false);

        TextView modalHeadingView = (TextView) alert.findViewById(R.id.modal_heading_title);
        TextView titleView = (TextView) alert.findViewById(R.id.title);
        TextView messageView = (TextView) alert.findViewById(R.id.message);
        final VZWRedButtonLargeRightArrow primaryBtn = (VZWRedButtonLargeRightArrow) alert.findViewById(R.id.primaryButton);
        VZWGrayButtonLarge secondaryBtn = (VZWGrayButtonLarge) alert.findViewById(R.id.secondaryButton);
        TextView closeIconView = (TextView)alert.findViewById(R.id.modal_close_icon);

        if(modalTitle!=null && modalTitle.length()>0) {
            modalHeadingView.setText(modalTitle);
        } else {
            alert.findViewById(R.id.modal_heading).setVisibility(View.GONE);
        }
        if(contentTitle!=null && contentTitle.length()>0) {
            titleView.setText(contentTitle);
        } else {
            titleView.setVisibility(View.GONE);
        }
        if(contentMessage!=null && contentMessage.length()>0) {
            messageView.setText(contentMessage);
        } else {
            messageView.setVisibility(View.GONE);
        }
        closeIconView.setVisibility(View.GONE);

        if(positiveButtonTitle!=null && positiveButtonTitle.length()>0) {
            primaryBtn.setText(positiveButtonTitle);
        }
        primaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (positiveListener != null) {
                    positiveListener.onClick(alert, 0);
                }
                if(alert!=null) {
                    alert.dismiss();
                }
            }
        });

        if(negativeButtonTitle!=null && negativeButtonTitle.length()>0) {
            secondaryBtn.setText(negativeButtonTitle);
        }
        secondaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (negativeListener != null) {
                    negativeListener.onClick(alert, 1);
                }
                if(alert!=null) {
                    alert.dismiss();
                }
            }
        });
    }*/

    public static void updateActionBarTitle(String s, Activity activity) {
        String appPkg = activity.getApplicationContext().getPackageName();
        try {
            Resources resources = activity.getApplicationContext().getPackageManager().getResourcesForApplication(appPkg);
            int resID = resources.getIdentifier("mvm_actionbar_textview", "id", appPkg);
            TextView appBarTextView = (TextView) activity.findViewById(resID);
            appBarTextView.setText(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateActionBarFirstIcon(int drawableResID, Activity activity) {
        String appPkg = activity.getApplicationContext().getPackageName();
        try {
            Resources resources = activity.getApplicationContext().getPackageManager().getResourcesForApplication(appPkg);
            int resID = resources.getIdentifier("mvmmenu", "id", appPkg);
            ImageView appBarFirstView = (ImageView) activity.findViewById(resID);
            appBarFirstView.setImageResource(drawableResID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int generateViewId() {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            for (;;) {
                final int result = sNextGeneratedId.get();
                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                int newValue = result + 1;
                if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
                if (sNextGeneratedId.compareAndSet(result, newValue)) {
                    return result;
                }
            }
        } else {
            return View.generateViewId();
        }
    }
}