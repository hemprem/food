package com.food.password.provider;


import android.util.Log;

import com.food.R;
import com.food.core.service.PDHAPIProviderBase;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHStringUtil;
import com.food.core.utils.PDHURLUtils;
import com.food.password.model.ChangePasswordRequestModel;
import com.food.password.model.ChangePasswordResponseModel;

import com.food.password.service.ChangePasswordAPIService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by agu186 on 2/20/2016.
 */
public class ChangePasswordProvider extends PDHAPIProviderBase {


    public static boolean lastRequestSuccess = true;

    private ChangePasswordRequestModel loginRequest;

    public String jsonName = null;

    public ChangePasswordProvider(ChangePasswordRequestModel loginRequest) {
     this.loginRequest = loginRequest;
    }

    @Override
   public void loadDataFromServer() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appContext.getString(R.string.host_url))
                .addConverterFactory(converterFactory)
                .build();
        ChangePasswordAPIService service = retrofit.create(ChangePasswordAPIService.class);
        String jsonData = PDHURLUtils.JSONData(loginRequest, ChangePasswordRequestModel.class, PDHAppState.getInstance().encodeInputJSON);
        Log.d("API:Req:getMTNDetails", jsonData);
        Call<ChangePasswordResponseModel> call = service.getLogin(loginRequest);
        final ChangePasswordResponseModel model = null;
        call.enqueue(new Callback<ChangePasswordResponseModel>() {
            @Override
            public void onResponse(Response<ChangePasswordResponseModel> response, Retrofit var) {
                ChangePasswordResponseModel model = response.body();
                String errorString = null;
                if (null == model) {
                    errorString = "INVALID Response:Check Connection";
                } else if (!PDHConstants.SERVER_SUCCESS_CODE.equals(model.Result.ErrorCode)) {
                    Log.e("API:Resp:getMTNDetails", model.toString());

                    errorString = PDHStringUtil.createServerErrorString(model.Result.ErrorCode, model.Result.ErrorMsg, null);
                    model = null;
                }
                callback.onResponse(errorString, model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("app", "error" + t.toString());
                ChangePasswordProvider.lastRequestSuccess = false;
                callback.onResponse(t.toString(), null);
            }
        });
    }



    @Override
    public void loadDataFromCache() {
        try {
            InputStream is = appContext.getAssets().open("jsons/getMTNDetails.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String bufferString = new String(buffer);
            Gson gson = new Gson();
            ChangePasswordResponseModel model = gson.fromJson(bufferString, ChangePasswordResponseModel.class);
            callback.onResponse(null, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
