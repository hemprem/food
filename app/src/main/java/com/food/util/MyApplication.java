package com.food.util;

import android.app.Application;

/**
 * Author by hemendra.kumar on 12/16/2015.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.setDefaultFont(this, "DEFAULT", "fonts/skia-regular.ttf");
        TypefaceUtil.setDefaultFont(this, "MONOSPACE", "fonts/skia-regular.ttf");
        TypefaceUtil.setDefaultFont(this, "SERIF", "fonts/skia-regular.ttf");
        TypefaceUtil.setDefaultFont(this, "SANS_SERIF", "fonts/skia-regular.ttf");
    }

}
