package com.food.util;

/**
 * Author by hemendra.kumar on 4/5/2016.
 */
public class ApplicationConstants {

    public static final String LOGIN_USER_NAME="LOGIN_USER_NAME";
    public static final String LOGIN_USER_EMAIL ="LOGIN_USER_EMAIL";

    public static final String LOGIN_STATUS="LOGIN_STATUS";

    public static final String ADD_ACTIVITY_TITLE="ADD_ACTIVITY_TITLE";
}
