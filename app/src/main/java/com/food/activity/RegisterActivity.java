package com.food.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.food.R;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.registration.model.RegistrationRequestModel;
import com.food.registration.model.RegistrationResponseModel;
import com.food.registration.provider.RegistrationProvider;

/**
 * Author by hemendra.kumar on 12/31/2015.
 */
public class RegisterActivity extends AppCompatActivity implements PDHAPIProviderCallback{

    private Toolbar mToolbar;
    private EditText edtName;
    private EditText edtEmail;
    private EditText edtMobile;
    private EditText edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        PDHAppState.getInstance().applicationContext =getApplicationContext();
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText(R.string.activity_register_title_text);

        edtName = (EditText)findViewById(R.id.activity_register_full_name_editText);
        edtEmail = (EditText)findViewById(R.id.activity_register_email_editText);
        edtMobile = (EditText)findViewById(R.id.activity_register_mobileNo_editText);
        edtPassword = (EditText)findViewById(R.id.activity_register_password_editText);
//        edtName.setText("jst");
//        edtEmail.setText("jst@s.c");
//        edtMobile.setText("123456");
//        edtPassword.setText("qwerty");
    }


    private RegistrationRequestModel createRegistrationRequest() {
        RegistrationRequestModel request = null;
        if (edtName.getText().toString() != null && edtEmail.getText().toString() != null && edtMobile.getText().toString()!=null && edtPassword.getText().toString()!=null) {
            request = new RegistrationRequestModel (edtName.getText().toString(),edtEmail.getText().toString(),edtMobile.getText().toString(),edtName.getText().toString(),edtPassword.getText().toString());
        }
        return request;
    }

    private void Registration(RegistrationRequestModel registrationRequest) {
        RegistrationProvider provider = new RegistrationProvider(registrationRequest);
        provider.setCallback(RegisterActivity.this);
        provider.appContext = RegisterActivity.this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void updateModel(RegistrationResponseModel model){
        if(model.Result.ErrorCode.equals("0")){
            Intent i = new Intent(RegisterActivity.this,LoginActivity.class);
            startActivity(i);
        }
    }




    public void onclickHandler(View view) {
        switch (view.getId()) {
            case R.id.registrationButton:
                // startActivity(new Intent(this, HomeActivity.class));
                Registration(createRegistrationRequest());
                break;
            default:
                break;
        }
    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof RegistrationResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((RegistrationResponseModel) model);
            }
        }
    }
}
