package com.food.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.food.R;
import com.food.adapter.AddressListAdapter;
import com.food.address.model.AddAddressRequestModel;
import com.food.address.model.AddAddressResponseModel;
import com.food.address.model.DeleteAddressRequestModel;
import com.food.address.model.DeleteAddressResponseModel;
import com.food.address.model.EditAddressRequestModel;
import com.food.address.model.EditAddressResponseModel;
import com.food.address.model.GetAddressListResponseModel;
import com.food.address.provider.AddToAddressProvider;
import com.food.address.provider.DeleteAddressProvider;
import com.food.address.provider.EditToAddressProvider;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.util.ApplicationConstants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Moni gupta on 15-05-2016.
 */
public class NewAddressActivity extends AppCompatActivity implements PDHAPIProviderCallback, View.OnClickListener {
    private static final String TAG = "NewAddressActivity";
    private Toolbar mToolbar;
    private AddressListAdapter adapter;
    private EditText edtAddressFullName;
    private EditText edtAddressFirstLine;
    private EditText edtAddressSecondLine;
    private EditText edtAddressLocality;
    private EditText edtAddressPinCode;
    private EditText edtAddressMobNo;
    private Button btnSave;
    private String id;
    private String uid;
    private String action;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newaddress);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);

        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);
        btnSave =(Button)findViewById(R.id.btnSave);
        edtAddressFullName =(EditText)findViewById(R.id.activity_newaddress_full_name_editText);
        edtAddressFirstLine =(EditText)findViewById(R.id.activity_newaddress_address_firstline_editText);
        edtAddressSecondLine =(EditText)findViewById(R.id.activity_newaddress_address_second_line_editText);
        edtAddressLocality =(EditText)findViewById(R.id.activity_newaddress_locality_editText);
        edtAddressPinCode =(EditText)findViewById(R.id.activity_newaddress_pincode_editText);
        edtAddressMobNo =(EditText)findViewById(R.id.activity_newaddress_mobileNo_editText);

        titleTv.setText(getIntent().getStringExtra(ApplicationConstants.ADD_ACTIVITY_TITLE));
        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);
        PDHAppState.getInstance().applicationContext =getApplicationContext();

        if(getIntent().getBundleExtra("BUN")!=null){
            action = getIntent().getBundleExtra("BUN").getString("ACTION");
            if(action.equals("ADD")){
                titleTv.setText("Add Address");
                btnSave.setText("SAVE");
                uid = getIntent().getBundleExtra("BUN").getString("UID");
            }else{
                titleTv.setText("Edit Address");
                btnSave.setText("EDIT");
                id = getIntent().getBundleExtra("BUN").getString("ID");
                uid = getIntent().getBundleExtra("BUN").getString("UID");
                edtAddressFullName.setText(getIntent().getBundleExtra("BUN").getString("FULL_NAME"));
                edtAddressFirstLine.setText(getIntent().getBundleExtra("BUN").getString("ADDRESS_LINE"));
                edtAddressLocality.setText(getIntent().getBundleExtra("BUN").getString("LOCALITY"));
                edtAddressPinCode.setText(getIntent().getBundleExtra("BUN").getString("PIN_CODE"));
                edtAddressMobNo.setText(getIntent().getBundleExtra("BUN").getString("MOBILE"));
            }

        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(action.equals("ADD")){
                    addAddress();
                }else{
                    editAddress();
                }
            }
        });
        // getAddressList();
           // editAddress();
    }

    public void addAddress(){
        AddToAddressProvider provider = new AddToAddressProvider(addAddressRequest());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    private AddAddressRequestModel addAddressRequest() {
        return new AddAddressRequestModel ("3",edtAddressFullName.getText().toString(),edtAddressFirstLine.getText().toString(),
                edtAddressLocality.getText().toString(),edtAddressPinCode.getText().toString(), edtAddressMobNo.getText().toString()
        );
    }

    public void editAddress(){
        EditToAddressProvider provider = new EditToAddressProvider(editAddressRequest());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }
    private EditAddressRequestModel editAddressRequest() {
        return new EditAddressRequestModel(id,"3",edtAddressFullName.getText().toString(),edtAddressFirstLine.getText().toString(),
                edtAddressLocality.getText().toString(),edtAddressPinCode.getText().toString(), edtAddressMobNo.getText().toString()
        );
    }

    @Override
    public void onClick(View v) {

    }

    public void updateModel(EditAddressResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            Intent data = new Intent();
            Bundle bun = new Bundle();
            bun.putString("ID",id);
            bun.putString("UID",uid);
            bun.putString("FULL_NAME",edtAddressFullName.getText().toString());
            bun.putString("ADDRESS_LINE",edtAddressFirstLine.getText().toString());
            bun.putString("LOCALITY",edtAddressLocality.getText().toString());
            bun.putString("PIN_CODE",edtAddressPinCode.getText().toString());
            bun.putString("MOBILE",edtAddressMobNo.getText().toString());
            data.putExtra("BUN", bun);
            setResult(RESULT_OK, data);
            finish();
        }
    }
    public void updateAddAddressModel(AddAddressResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof EditAddressResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((EditAddressResponseModel) model);
            }
        } else if (model instanceof AddAddressResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateAddAddressModel((AddAddressResponseModel) model);
            }
        }

    }
}
