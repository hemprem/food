package com.food.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.food.R;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.login.model.LoginRequestModel;
import com.food.login.model.LoginResponseModel;
import com.food.login.provider.LoginProvider;
import com.food.util.AppPreferences;

/**
 * Author by hemendra.kumar on 12/16/2015.
 */
public class LoginActivity extends AppCompatActivity  implements PDHAPIProviderCallback, OnClickListener {

    private static final String TAG = "LoginActivity";
    private Toolbar mToolbar;
    private EditText edtLogin, edtPassword;
    private Button loginBtn;
    SharedPreferences prefs;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        prefs= PreferenceManager.getDefaultSharedPreferences(this);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText("Login");
        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);
        edtLogin = (EditText) findViewById(R.id.activity_login_mobileNo_editText);
        edtPassword = (EditText) findViewById(R.id.activity_login_password_editText);
        loginBtn =(Button)findViewById(R.id.activity_login_login_btn);
        loginBtn.setOnClickListener(this);
        PDHAppState.getInstance().applicationContext =getApplicationContext();
        edtLogin.setText("alok@gmail.co");
        edtPassword.setText("dumbledore");
    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof LoginResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((LoginResponseModel) model);
            }
        }
    }

    private LoginRequestModel createLoginRequest() {
        LoginRequestModel request = null;
        if (edtLogin.getText().toString() != null && edtPassword.getText().toString() != null) {
            request = new LoginRequestModel(edtLogin.getText().toString(), edtPassword.getText().toString());
        }
        return request;
    }

    private void Login(LoginRequestModel loginRequest) {
        LoginProvider provider = new LoginProvider(loginRequest);
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void updateModel(LoginResponseModel model) {
      if(model.Result.ErrorMsg.equals("Success")){
          AppPreferences.saveUserEmail(prefs, model.Result.Email);
          AppPreferences.saveUserName(prefs, model.Result.Name);
          AppPreferences.saveLoginStatus(prefs, true);
          Intent i = new Intent(LoginActivity.this,HomeActivity.class);
          startActivity(i);
          finish();
      }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_login_login_btn:
                // startActivity(new Intent(this, HomeActivity.class));

                Login(createLoginRequest());

//                Intent i = new Intent(LoginActivity.this,HomeActivity.class);
//                startActivity(i);
//                finish();
                break;
            default:
                break;
        }
    }
}