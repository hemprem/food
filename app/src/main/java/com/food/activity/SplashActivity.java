package com.food.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.food.R;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.socialLogin.model.SocialLoginRequestModel;
import com.food.socialLogin.model.SocialLoginResponseModel;
import com.food.socialLogin.provider.SocialLoginProvider;
import com.food.util.AppPreferences;

import org.json.JSONObject;

import java.util.Arrays;

public class SplashActivity extends AppCompatActivity implements PDHAPIProviderCallback {

    private CallbackManager callbackManager;
    private String FACEBOOK_FIRST_NAME, FACEBOOK_LAST_NAME, FACEBOOK_EMAIL,FACEBOOK_PROVIDER_ID;
    private String FACEBOOK_PROVIDER="Facebook";
    private ProgressDialog fbProgressDialog;
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_splash);
        callbackManager = CallbackManager.Factory.create();
        PDHAppState.getInstance().applicationContext =getApplicationContext();
        prefs= PreferenceManager.getDefaultSharedPreferences(this);

        //if already login
        if (AppPreferences.getLoginStatus(prefs)){
            startActivity(new Intent(SplashActivity.this,HomeActivity.class));
            finish();
        }

    LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            System.out.println("Facebook Login Successful");
            System.out.println("User ID  : " + loginResult.getAccessToken().getUserId());
            System.out.println("Authentication Token : " + loginResult.getAccessToken().getToken());

            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject user, GraphResponse graphResponse) {

                    if (user != null) {
                        FACEBOOK_FIRST_NAME = Profile.getCurrentProfile().getFirstName();
                        FACEBOOK_EMAIL = user.optString("email");
                        FACEBOOK_PROVIDER_ID =Profile.getCurrentProfile().getId();

                    }

                    System.out.println("User Details is  : " + Profile.getCurrentProfile().getFirstName() + " Email : " + FACEBOOK_EMAIL + " Provider Id : " + FACEBOOK_PROVIDER_ID);
                    fbProgressDialog.cancel();
                    //Sending to another activity
                    SocialLogin(createSocialLoginRequest());

                }

            });

            Bundle parameters1 = new Bundle();
            parameters1.putString("fields", "id,name,email");
            request.setParameters(parameters1);
            request.executeAsync();
            // Toast.makeText(LoginActivity.this, "Login Successful!", Toast.LENGTH_LONG).show();

        }

        @Override
        public void onCancel() {
            Toast.makeText(SplashActivity.this, "Login cancelled by user", Toast.LENGTH_LONG).show();
            System.out.println("Facebook Login failed!!");
            fbProgressDialog.cancel();

        }

        @Override
        public void onError(FacebookException e) {
            Toast.makeText(SplashActivity.this, SplashActivity.this.getResources().getString(R.string.default_error), Toast.LENGTH_LONG).show();
            System.out.println("Facebook Login failed!!");
            fbProgressDialog.cancel();
        }
    });


    }

    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent i) {
        callbackManager.onActivityResult(reqCode, resCode, i);
    }

    private SocialLoginRequestModel createSocialLoginRequest() {
        SocialLoginRequestModel request = null;
        if (FACEBOOK_FIRST_NAME != null && FACEBOOK_EMAIL != null && FACEBOOK_PROVIDER_ID != null && FACEBOOK_PROVIDER != null) {
            request = new SocialLoginRequestModel(FACEBOOK_FIRST_NAME,FACEBOOK_EMAIL,FACEBOOK_PROVIDER_ID,FACEBOOK_PROVIDER);
        }
        return request;
    }

    private void SocialLogin(SocialLoginRequestModel socialLoginRequest) {
        SocialLoginProvider socialLoginProvider = new SocialLoginProvider(socialLoginRequest);
        socialLoginProvider.setCallback(SplashActivity.this);
        socialLoginProvider.appContext=SplashActivity.this;
        PDHViewUtils.getInstance().showProgress(this);
        socialLoginProvider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void updateModel(SocialLoginResponseModel model) {
        if(model.Result.ErrorMsg.equals("Success")){
            AppPreferences.saveUserEmail(prefs, model.Result.User.name);
            AppPreferences.saveLoginStatus(prefs, true);
            Intent i = new Intent(SplashActivity.this,HomeActivity.class);
            startActivity(i);
            finish();
        }
    }




    public void onclickHandler(View view) {

        switch (view.getId()) {
            case R.id.activity_splash_register_button:
                startActivity(new Intent(this, RegisterActivity.class));
                finish();
                break;
            case R.id.activity_splash_login_button:
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
            case R.id.activity_splash_fb_button:
                new FacebookLogin().execute();
                break;

            default:
                break;
        }

    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof SocialLoginResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((SocialLoginResponseModel) model);
            }
        }
    }


    private class FacebookLogin extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            fbProgressDialog = ProgressDialog.show(SplashActivity.this, "", "");
            fbProgressDialog.setIndeterminate(true);
            fbProgressDialog.setContentView(R.layout.custom_progress_bar);
            fbProgressDialog.setCancelable(false);
            fbProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            fbProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            LoginManager.getInstance().logInWithReadPermissions(SplashActivity.this, Arrays.asList("public_profile", "email"));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            fbProgressDialog.cancel();
        }
    }


    //Track App Installs and App Opens
    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }


    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }
}
