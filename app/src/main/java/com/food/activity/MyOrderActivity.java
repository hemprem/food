package com.food.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.food.R;
import com.food.adapter.MyOrderAdapter;
import com.food.cart.model.GetCartDetailRequestModel;
import com.food.cart.model.GetCartDetailResponseModel;
import com.food.cart.provider.GetCartDetailProvider;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.util.AppPreferences;
import com.food.util.ApplicationConstants;

import java.util.ArrayList;

/**
 * Created by agu186 on 3/20/2016.
 */
public class MyOrderActivity  extends AppCompatActivity  implements PDHAPIProviderCallback, View.OnClickListener{
    private static final String TAG = "MyOrderActivity";
    private MyOrderAdapter adapter;
    ListView listView;
    private Toolbar mToolbar;
    private ArrayList<GetCartDetailResponseModel.Output.RecordList> itemsArray;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prefs= PreferenceManager.getDefaultSharedPreferences(this);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText("ORDER HISTORY");
        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);
        itemsArray = new ArrayList<GetCartDetailResponseModel.Output.RecordList>();
        listView = (ListView) findViewById(R.id.activity_my_order_listview);
        adapter = new MyOrderAdapter(this,itemsArray);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        getCartDetail();
    }

    private GetCartDetailRequestModel getCartDetailRequestModel() {
        GetCartDetailRequestModel request = null;
        return new GetCartDetailRequestModel(AppPreferences.getUserEmail(prefs));
    }


    public void getCartDetail(){
        GetCartDetailProvider provider = new GetCartDetailProvider(getCartDetailRequestModel());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }


    public void onclickHandler(View view) {

        switch (view.getId()) {
            case R.id.activity_order_history_selected_address_btn:
                startActivity(new Intent(MyOrderActivity.this,AddressBookActivity.class)
                        .putExtra(ApplicationConstants.ADD_ACTIVITY_TITLE,"Choose Delivery Address"));

                break;
            default:
                break;
        }

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof GetCartDetailResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((GetCartDetailResponseModel) model);
            }
        }
    }

    public void updateModel(GetCartDetailResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            itemsArray =(ArrayList<GetCartDetailResponseModel.Output.RecordList>) model.Result.Record;
            adapter = new MyOrderAdapter(this,itemsArray);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
}
