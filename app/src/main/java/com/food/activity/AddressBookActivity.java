package com.food.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.food.R;
import com.food.adapter.AddressListAdapter;
import com.food.address.model.DeleteAddressRequestModel;
import com.food.address.model.DeleteAddressResponseModel;
import com.food.address.model.GetAddressListRequestModel;
import com.food.address.model.GetAddressListResponseModel;
import com.food.address.provider.DeleteAddressProvider;
import com.food.address.provider.GetAddressListProvider;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.placeOrder.model.PlaceOrderRequestModel;
import com.food.placeOrder.model.PlaceOrderResponseModel;
import com.food.placeOrder.provider.PlaceOrderProvider;
import com.food.util.AppPreferences;
import com.food.util.ApplicationConstants;

import java.util.ArrayList;

/**
 * Author by hemendra.kumar on 1/8/2016.
 */
public class AddressBookActivity extends AppCompatActivity implements PDHAPIProviderCallback, View.OnClickListener{

    private static final String TAG = "AddressBookActivity";
    private Toolbar mToolbar;
    private AddressListAdapter adapter;
    private LinearLayout activity_addresssbook_add_address_lyt;
    ListView listView;
    ArrayList<GetAddressListResponseModel.Output.RecordList> addressListItems;
    int deletePos =-1;
    int edtPos =-1;
    SharedPreferences prefs;
    TextView priceTv;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addressbook);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        prefs= PreferenceManager.getDefaultSharedPreferences(this);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        priceTv = (TextView) findViewById(R.id.activity_addressbook_price_TV);
        titleTv.setText(getIntent().getStringExtra(ApplicationConstants.ADD_ACTIVITY_TITLE));
        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);
        activity_addresssbook_add_address_lyt =(LinearLayout)findViewById(R.id.activity_addresssbook_add_address_lyt);
        PDHAppState.getInstance().applicationContext =getApplicationContext();
        addressListItems = new ArrayList<GetAddressListResponseModel.Output.RecordList>();
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        getAddressList();
        activity_addresssbook_add_address_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            addAddress();
            }
        });
    }

    public void getAddressList(){
        GetAddressListProvider provider = new GetAddressListProvider(getAddressListRequest());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void placeOrder(){
        PlaceOrderProvider provider = new PlaceOrderProvider(getPlaceOrderListRequest());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void deleteAddress( int pos){
        deletePos =pos;
        DeleteAddressProvider provider = new DeleteAddressProvider(deleteAddressRequest(pos));
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void addAddress(){
        Intent i = new Intent(this, NewAddressActivity.class);
        Bundle bun = new Bundle();
        bun.putString("ACTION","ADD");
        bun.putString("UID","3");
        i.putExtra("BUN", bun);
        startActivityForResult(i, 200);
    }

    public void editAddress( int pos){
        edtPos =pos;
        Intent i = new Intent(this, NewAddressActivity.class);
        Bundle bun = new Bundle();
        bun.putString("ACTION","EDIT");
        bun.putString("ID",addressListItems.get(pos).Id);
        bun.putString("UID","3");
        bun.putString("FULL_NAME",addressListItems.get(pos).Name);
        bun.putString("ADDRESS_LINE",addressListItems.get(pos).Address);
        bun.putString("LOCALITY",addressListItems.get(pos).Locality);
        bun.putString("PIN_CODE",addressListItems.get(pos).Pincode);
        bun.putString("MOBILE",addressListItems.get(pos).Mobile);
        i.putExtra("BUN",bun);
        startActivityForResult(i, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if(resultCode == Activity.RESULT_OK){
                if(data.getBundleExtra("BUN")!=null){
                    if(edtPos!=-1) {
                        GetAddressListResponseModel.Output.RecordList record  = addressListItems.get(edtPos);
                        record.Name =  data.getBundleExtra("BUN").getString("FULL_NAME");
                        record.Address = data.getBundleExtra("BUN").getString("ADDRESS_LINE");
                        record.Locality = data.getBundleExtra("BUN").getString("LOCALITY");
                        record.Pincode = data.getBundleExtra("BUN").getString("PIN_CODE");
                        record.Mobile = data.getBundleExtra("BUN").getString("MOBILE");
                        addressListItems.set(edtPos,record);
                        adapter = new AddressListAdapter(AddressBookActivity.this,addressListItems);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
                //Write your code if there's no result
            }else if (requestCode == 200){
            if(resultCode == Activity.RESULT_OK){
               getAddressList();
            }

        }

    }

    private GetAddressListRequestModel getAddressListRequest() {
        return new GetAddressListRequestModel("3");
    }

    private DeleteAddressRequestModel deleteAddressRequest(int pos) {
        return new DeleteAddressRequestModel(addressListItems.get(pos).Id,"3");
    }
    private PlaceOrderRequestModel getPlaceOrderListRequest() {
        String uid= AppPreferences.getUserEmail(prefs);
        String total_price=priceTv.getText().toString();
        String discount = "10%";
        String comment = "awesome food";
        String address= addressListItems.get(0).Address+",  "
                +addressListItems.get(0).Locality+"-"
                +addressListItems.get(0).Pincode+" Mob Number -"+addressListItems.get(0).Mobile;

        //new PlaceOrderRequestModel("3");
        return null;
    }



    public void onclickHandler(View view) {

        switch (view.getId()) {
            case R.id.activity_addresssbook_place_order_btn:
               // placeOrder();
               startActivity(new Intent(AddressBookActivity.this,ThankYouActivity.class));
                break;
            default:
                break;
        }

    }

    @Override
    public void onClick(View v) {

    }

    public void updateModel(GetAddressListResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            addressListItems = model.Result.Record;
            adapter = new AddressListAdapter(AddressBookActivity.this,addressListItems);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    public void updatePlaceOrderModel(PlaceOrderResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            startActivity(new Intent(AddressBookActivity.this,ThankYouActivity.class));
        }
    }

    public void updateDeleteAddressModel(DeleteAddressResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
           if(deletePos!=-1){
               addressListItems.remove(deletePos);
               adapter = new AddressListAdapter(AddressBookActivity.this,addressListItems);
               listView.setAdapter(adapter);
               adapter.notifyDataSetChanged();
           }
        }
    }


    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof GetAddressListResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((GetAddressListResponseModel) model);
            }
        }
            else if (model instanceof DeleteAddressResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateDeleteAddressModel((DeleteAddressResponseModel) model);
            }
        }
        else if (model instanceof PlaceOrderResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updatePlaceOrderModel((PlaceOrderResponseModel) model);
            }
        }
    }
}