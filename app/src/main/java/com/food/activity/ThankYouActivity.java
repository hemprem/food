package com.food.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.food.R;

/**
 * Author by hemendra.kumar on 4/5/2016.
 */
public class ThankYouActivity extends AppCompatActivity {

    private static final String TAG = "ThankYouActivity";
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);//disable back button
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText("Thank You");
      //  titleTv.setGravity(Gravity.CENTER_HORIZONTAL);


    }

    public void onclickHandler(View view) {

        switch (view.getId()) {
            case R.id.activity_thankyou_back_home_btn:
                startActivity(new Intent(ThankYouActivity.this,HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                break;
            default:
                break;
        }

    }
}