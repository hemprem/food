package com.food.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.food.R;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.password.model.ChangePasswordRequestModel;
import com.food.password.model.ChangePasswordResponseModel;
import com.food.password.provider.ChangePasswordProvider;
import com.food.util.AppPreferences;

/**
 * Created by alok on 05/06/16.
 */
public class ChangePasswordActivity extends AppCompatActivity implements PDHAPIProviderCallback, View.OnClickListener  {

    SharedPreferences prefs;
    EditText oldPass, newPass, confirmPass;
    TextView btnSubmit;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_change_password);
        prefs= PreferenceManager.getDefaultSharedPreferences(this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText("CHANGE PASSWORD");
        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);
        oldPass = (EditText) findViewById(R.id.dailog_chnagepass_oldpass_editText);
        newPass = (EditText) findViewById(R.id.dailog_chnagepass_newpass_editText);
        confirmPass = (EditText) findViewById(R.id.dailog_chnagepass_confirmpass_editText);
        btnSubmit = (TextView) findViewById(R.id.dailog_chnagepass_submit_textview);
        btnSubmit.setOnClickListener(this);
        PDHAppState.getInstance().applicationContext =getApplicationContext();


    }


    private ChangePasswordRequestModel createChangePasswordRequest() {
        ChangePasswordRequestModel request = null;
        if (oldPass.getText().toString() != null && newPass.getText().toString() != null) {
            request = new ChangePasswordRequestModel(AppPreferences.getUserEmail(prefs),oldPass.getText().toString(), newPass.getText().toString());
            //request = new ChangePasswordRequestModel("3",oldPass.getText().toString(), newPass.getText().toString());
        }
        return request;
    }

    private void chnagePassword(ChangePasswordRequestModel loginRequest) {
        ChangePasswordProvider provider = new ChangePasswordProvider(loginRequest);
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void updateModel(ChangePasswordResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            setResult(100);
            Toast.makeText(this,model.Result.ErrorMsg.toString(),Toast.LENGTH_SHORT);
            finish();
        } else {
            Toast.makeText(this,model.Result.ErrorMsg.toString(),Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof ChangePasswordResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((ChangePasswordResponseModel) model);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dailog_chnagepass_submit_textview:
                // startActivity(new Intent(this, HomeActivity.class));
                chnagePassword(createChangePasswordRequest());
                break;
            default:
                break;
        }
    }
}
