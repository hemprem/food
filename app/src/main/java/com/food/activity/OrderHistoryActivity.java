package com.food.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.food.R;
import com.food.adapter.OrderHistoryAdapter;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.orderHistory.model.OrderHistoryRequestModel;
import com.food.orderHistory.model.OrderHistoryResponseModel;
import com.food.orderHistory.provider.OrderHistoryProvider;
import com.food.profile.model.GetUserProfileRequestModel;
import com.food.profile.model.GetUserProfileResponseModel;
import com.food.profile.provider.GetUserProfileProvider;
import com.food.util.AppPreferences;

import java.util.ArrayList;

/**
 * Author by hemendra.kumar on 1/8/2016.
 */
public class OrderHistoryActivity extends AppCompatActivity implements PDHAPIProviderCallback, View.OnClickListener  {

    private static final String TAG = "LoginActivity";
    private Toolbar mToolbar;
    SharedPreferences preferences;

    private OrderHistoryAdapter adapter;
    ListView listView;
    ArrayList<OrderHistoryResponseModel.Output.RecordData> addressListItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText("ORDER HISTORY");
        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        addressListItems = new ArrayList<OrderHistoryResponseModel.Output.RecordData>();
        listView = (ListView) findViewById(R.id.activity_order_history_listview);
        adapter = new OrderHistoryAdapter(this, addressListItems);
        listView.setAdapter(adapter);
        getOrderHistory(createOrderListRequest());
    }
    public void getOrderHistory(OrderHistoryRequestModel requestModel){
        OrderHistoryProvider provider = new OrderHistoryProvider(requestModel);
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    private OrderHistoryRequestModel createOrderListRequest() {
        OrderHistoryRequestModel request = new OrderHistoryRequestModel("03");
        return request;
    }
    public void onclickHandler(View view) {

        switch (view.getId()) {
            case 1:
                break;
            default:
                break;
        }

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof OrderHistoryResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((OrderHistoryResponseModel) model);
            }
        }
    }


    public void updateModel(OrderHistoryResponseModel model_data) {
        if(model_data.Result.ErrorCode.equals("0")){
            addressListItems =  model_data.Result.Record;
            adapter = new OrderHistoryAdapter(this, addressListItems);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

}