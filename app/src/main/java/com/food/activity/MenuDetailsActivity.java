package com.food.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.R;
import com.food.cart.model.AddItemToCartResponseModel;
import com.food.cart.model.EditItemToCartResponseModel;
import com.food.cart.model.GetCartDetailRequestModel;
import com.food.cart.model.GetCartDetailResponseModel;
import com.food.cart.provider.GetCartDetailProvider;
import com.food.categoryMenuList.model.MenuListResponseModel;
import com.food.categoryMenuList.provider.MenuListProvider;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.fragment.MenuAllFragment;
import com.food.profile.model.EditUserProfileResponseModel;
import com.food.profile.model.GetUserProfileRequestModel;
import com.food.profile.provider.GetUserProfileProvider;
import com.food.searchFood.model.SearchFoodModel;
import com.food.searchFood.provider.SearchFoodProvider;
import com.food.util.AppPreferences;
import com.food.util.LockableViewPager;

import java.util.ArrayList;

/**
 * Author by hemendra.kumar on 1/12/2016.
 */
public class MenuDetailsActivity extends AppCompatActivity implements PDHAPIProviderCallback  {

    private ArrayList<MenuListResponseModel.Output.RecordList> itemsArray;
    private ArrayList<MenuListResponseModel.Output.RecordList> itemsSearchFoodArray;
    private ArrayList<MenuListResponseModel.Output.RecordList> itemsVegArray;
    private ArrayList<MenuListResponseModel.Output.RecordList> itemsNonVegArray;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private Toolbar toolbar;
    private LockableViewPager mViewPager;
    MenuAllFragment frag;
    TabLayout tabLayout;
    private SearchView searchView;
    TextView totalPriceTv, totalItemTv;
    SharedPreferences prefs;
    RelativeLayout bottomLyt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        bottomLyt = (RelativeLayout) findViewById(R.id.activity_menuDetails_bottomLyt);
        totalItemTv = (TextView) findViewById(R.id.activity_menuDetails_dishitemTV);
        totalPriceTv = (TextView) findViewById(R.id.menu_details_price_TV);
        titleTv.setText("Chinese");
        itemsArray = new ArrayList<MenuListResponseModel.Output.RecordList>();
        itemsVegArray = new ArrayList<MenuListResponseModel.Output.RecordList>();
        itemsNonVegArray = new ArrayList<MenuListResponseModel.Output.RecordList>();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (LockableViewPager) findViewById(R.id.container);
        mViewPager.setSwipeLocked(true);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==1) {
                    frag.setData(itemsVegArray);
                }else if(tab.getPosition()==2){
                    frag.setData(itemsNonVegArray);
                }else{
                    frag.setData(itemsArray);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if(tab.getPosition()==1) {
                    frag.setData(itemsVegArray);
                }else if(tab.getPosition()==2){
                    frag.setData(itemsNonVegArray);
                }else{
                    frag.setData(itemsArray);
                }
            }
        });

        getMenuData();
        getCartDetail();

    }
    public void getCartDetail() {
        GetCartDetailProvider provider = new GetCartDetailProvider(getCartDetailRequestModel());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    private GetCartDetailRequestModel getCartDetailRequestModel() {
        GetCartDetailRequestModel request = null;
        return new GetCartDetailRequestModel(AppPreferences.getUserEmail(prefs));
    }

    private void searchFood(String searchStr) {
        SearchFoodProvider provider = new SearchFoodProvider(searchStr);
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void getMenuData(){
        MenuListProvider provider = new MenuListProvider();
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void updateModel(GetCartDetailResponseModel model) {
        if (model.Result.ErrorCode.equals("0")) {
            String totalItem = model.Result.TotalItem;
            String totalPrice = model.Result.TotalPrice;
            EditBottomDialog( totalItem, totalPrice);
        }
    }

    public void onClickHandler(View view) {
        switch (view.getId())
        {
            case R.id.activity_menuDetails_bottomLyt:
                Intent intent = new Intent(MenuDetailsActivity.this,MyOrderActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof MenuListResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((MenuListResponseModel) model);
            }
        }else if (model instanceof AddItemToCartResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateAddToCart((AddItemToCartResponseModel) model);
            }
        }else if (model instanceof EditItemToCartResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateEditToCart((EditItemToCartResponseModel) model);
            }
        } else if (model instanceof GetCartDetailResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((GetCartDetailResponseModel) model);
            }
        }/*else if (model instanceof MenuListResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateSearchFoodModel((MenuListResponseModel) model);
            }
        }*/
    }
    private void EditBottomDialog(String dishItemValue, String priceTvValue) {
        bottomLyt.setVisibility(View.VISIBLE);
        if (priceTvValue != null)
            totalPriceTv.setText(priceTvValue);
        if (dishItemValue != null)
            totalItemTv.setText(new StringBuilder().append(dishItemValue).append(" dishes in your order ").toString());
    }
   public void updateSearchFoodModel(MenuListResponseModel model){
       if(model.Result.ErrorCode.equals("0")){
           itemsArray =(ArrayList<MenuListResponseModel.Output.RecordList>) model.Result.Record;
           for(int i=0;i<itemsArray.size();i++) {
               if (itemsArray.get(i).DishType.equals("Veg")) {
                   itemsVegArray.add(itemsArray.get(i));
               } else if (itemsArray.get(i).DishType.equals("NonVeg")) {
                   itemsNonVegArray.add(itemsArray.get(i));
               }
           }
           if(tabLayout.getSelectedTabPosition()==1) {
               frag.setData(itemsVegArray);
           }else if(tabLayout.getSelectedTabPosition()==2){
               frag.setData(itemsNonVegArray);
           }else{
               frag.setData(itemsArray);
           }
       }
    }


    public void updateModel(MenuListResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            searchView.setFocusable(false);
            itemsSearchFoodArray =(ArrayList<MenuListResponseModel.Output.RecordList>) model.Result.Record;

            if(itemsSearchFoodArray!=null && itemsSearchFoodArray.size()>0) {
                itemsArray = itemsSearchFoodArray;
                for (int i = 0; i < itemsArray.size(); i++) {
                    if (itemsArray.get(i).DishType.equals("Veg")) {
                        itemsVegArray.add(itemsArray.get(i));
                    } else if (itemsArray.get(i).DishType.equals("NonVeg")) {
                        itemsNonVegArray.add(itemsArray.get(i));
                    }
                }
                if (tabLayout.getSelectedTabPosition() == 1) {
                    frag.setData(itemsVegArray);
                } else if (tabLayout.getSelectedTabPosition() == 2) {
                    frag.setData(itemsNonVegArray);
                } else {
                    frag.setData(itemsArray);
                }
            }else{
                itemsVegArray.clear();
                itemsNonVegArray.clear();
                itemsArray.clear();
                if (tabLayout.getSelectedTabPosition() == 1) {
                    frag.setData(itemsVegArray);
                } else if (tabLayout.getSelectedTabPosition() == 2) {
                    frag.setData(itemsNonVegArray);
                } else {
                    frag.setData(itemsArray);
                }
            }
        }
    }

    public void updateAddToCart(AddItemToCartResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            getCartDetail();
        }
    }

    public void updateEditToCart(EditItemToCartResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            Intent i = new Intent(this, MyOrderActivity.class);
            startActivity(i);
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            if(position==0){
                frag  = new MenuAllFragment();
                Bundle bun = new Bundle();
                bun.putSerializable("MENUDATA", itemsArray);
                frag.setArguments(bun);
                return frag;
            }else if(position==1){
                frag  = new MenuAllFragment();
                Bundle bun = new Bundle();
                bun.putSerializable("MENUDATA",itemsArray);
                frag.setArguments(bun);
                return frag;
            } else {
                frag  = new MenuAllFragment();
                Bundle bun = new Bundle();
                bun.putSerializable("MENUDATA",itemsArray);
                frag.setArguments(bun);
                return frag;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "ALL";
                case 1:
                    return "VEG";
                case 2:
                    return "NON-VEG";
            }
            return null;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setQueryHint("Search View Hint");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {
                //Log.e("onQueryTextChange", "called");
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // Do your task here
                searchFood(query);
                return true;
            }

        });
       // searchView.setOnCloseListener(closeListener);

       // searchView.expandActionView();
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                getMenuData();
                return true;       // Return true to collapse action view
            }
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;      // Return true to expand action view
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        getMenuData();
        super.onOptionsMenuClosed(menu);
    }

    final SearchView.OnCloseListener closeListener = new SearchView.OnCloseListener() {

        @Override
        public boolean onClose() {
            getMenuData();
            return true;
        }
    };

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                searchFood();
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/

}
