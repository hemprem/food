package com.food.activity;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.food.R;
import com.food.adapter.MyOrderAdapter;
import com.food.adapter.NavigationAdapter;
import com.food.adapter.RecyclerViewAdapter;
import com.food.cart.model.AddItemToCartRequestModel;
import com.food.cart.model.AddItemToCartResponseModel;
import com.food.cart.model.GetCartDetailRequestModel;
import com.food.cart.model.GetCartDetailResponseModel;
import com.food.cart.provider.AddItemToCartProvider;
import com.food.cart.provider.GetCartDetailProvider;
import com.food.categoryMenuList.model.CategoryListResponseModel;
import com.food.categoryMenuList.provider.CategoryListProvider;
import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.util.AppPreferences;
import com.food.util.ApplicationConstants;
import com.food.util.ApplicationUtil;
import com.food.weeklyDish.model.WeeklyDishResponseModel;
import com.food.weeklyDish.provider.WeeklyDishListProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Author by hemendra.kumar on 12/16/2015.
 */
public class HomeActivity extends AppCompatActivity implements PDHAPIProviderCallback,View.OnClickListener {
    //First We Declare Titles And Icons For Our Navigation Drawer List View
    //This Icons And Titles Are holded in an Array as you can see

    String TITLES[] = {"My profile", "Address book", "Order history", "Rate this app", "Share this app", "About us", "Contact us"};
    int ICONS[] = {R.mipmap.my_profile, R.mipmap.address_book,
            R.mipmap.recent_history,
            R.mipmap.rate_app,
            R.mipmap.share,
            R.mipmap.about_us,
            R.mipmap.contact_us};
    int halfPriceQuantityText = 1;
    int fullPriceQuantityText = 1;
    private int BOTTOMDIALOG = 0;

//    //Similarly we Create a String Resource for the name and email in the header view
//    //And we also create a int resource for profile picture in the header view

    private Toolbar toolbar;                              // Declaring the Toolbar Object
    RecyclerViewAdapter rcAdapter;
    RecyclerView mRecyclerView;
    RecyclerView rView; // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;
    private GridLayoutManager lLayout;// Declaring DrawerLayout
    List<CategoryListResponseModel.Output.RecordList> menuCategoriesListItems;
    List<WeeklyDishResponseModel.Output.RecordList> weeklyDishItem;
    ActionBarDrawerToggle mDrawerToggle;
    TextView weeklyDishTV, totalPriceTv, totalItemTv;
    SharedPreferences prefs;
    private Context context;
    RelativeLayout bottomLyt;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);
        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_menu);
        setSupportActionBar(toolbar);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.context = this;
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText(R.string.activity_home_title_text);

        TextView userName = (TextView) findViewById(R.id.activity_pdh_home_userNameTV);
        bottomLyt = (RelativeLayout) findViewById(R.id.home_activity_bottom);
        totalItemTv = (TextView) findViewById(R.id.activity_pdh_home_dishitemTV);
        totalPriceTv = (TextView) findViewById(R.id.activity_pdh_home_price_TV);
        userName.setText("Hello " + AppPreferences.getUserName(prefs));
        weeklyDishTV = (TextView) findViewById(R.id.activity_pdh_home_weeklydishTV);
        ImageView imageView = (ImageView) findViewById(R.id.activity_pdh_profile_Imgview);
        imageView.setImageBitmap(ApplicationUtil.roundCornerImage(BitmapFactory.decodeResource(getResources(), R.mipmap.log), 50));
        getWeeklyDishData();

        menuCategoriesListItems = new ArrayList<CategoryListResponseModel.Output.RecordList>();
        weeklyDishItem = new ArrayList<WeeklyDishResponseModel.Output.RecordList>();
        lLayout = new GridLayoutManager(HomeActivity.this, 3);
        rView = (RecyclerView) findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        rcAdapter = new RecyclerViewAdapter(HomeActivity.this, menuCategoriesListItems);
        rView.setAdapter(rcAdapter);


        mRecyclerView = (RecyclerView) findViewById(R.id.nvView); // Assigning the RecyclerView Object to the xml View
        mRecyclerView.setHasFixedSize(true);  // Letting the system know that the list objects are of fixed size
        mAdapter = new NavigationAdapter(TITLES, ICONS);  // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        final GestureDetector mGestureDetector = new GestureDetector(HomeActivity.this, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });

        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                    Drawer.closeDrawers();
                    selectDrawerItem(recyclerView.getChildPosition(child));
                    Toast.makeText(HomeActivity.this, "The Item Clicked is: " + recyclerView.getChildPosition(child), Toast.LENGTH_SHORT).show();

                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });


        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager

        Drawer = (DrawerLayout) findViewById(R.id.drawer_layout);        // Drawer object Assigned to the view

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };

        mDrawerToggle.setDrawerIndicatorEnabled(false);
//          // Drawer Toggle Object Made
//          Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
//          mDrawerToggle.syncState();
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("", "Setting the toolbar nav icon click and hide default icon");
                Drawer.openDrawer(Gravity.LEFT);

            }
        });

        getCategoriesData();
        getCartDetail();
    }

    public void getCategoriesData() {
        CategoryListProvider provider = new CategoryListProvider();
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void getWeeklyDishData() {
        WeeklyDishListProvider provider = new WeeklyDishListProvider();
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    public void getCartDetail() {
        GetCartDetailProvider provider = new GetCartDetailProvider(getCartDetailRequestModel());
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    private GetCartDetailRequestModel getCartDetailRequestModel() {
        GetCartDetailRequestModel request = null;
        return new GetCartDetailRequestModel(AppPreferences.getUserEmail(prefs));
    }

    public void updateModel(CategoryListResponseModel model) {
        if (model.Result.ErrorCode.equals("0")) {
            menuCategoriesListItems = (ArrayList<CategoryListResponseModel.Output.RecordList>) model.Result.Record;
            rcAdapter = new RecyclerViewAdapter(HomeActivity.this, menuCategoriesListItems);
            rView.setAdapter(rcAdapter);
        }
    }

    public void updateModel(WeeklyDishResponseModel model) {
        if (model.Result.ErrorCode.equals("0")) {
            weeklyDishItem = (ArrayList<WeeklyDishResponseModel.Output.RecordList>) model.Result.Record;
            weeklyDishTV.setText(weeklyDishItem.get(0).Title);
        }
    }

    public void updateModel(GetCartDetailResponseModel model) {
        if (model.Result.ErrorCode.equals("0")) {
            String totalItem = model.Result.TotalItem;
            String totalPrice = model.Result.TotalPrice;
            EditBottomDialog( totalItem, totalPrice);
        }
    }

    public void selectDrawerItem(int menuItemPosition) {

        switch (menuItemPosition) {
            case 0:
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
                break;
            case 1:
                startActivity(new Intent(HomeActivity.this, AddressBookActivity.class)
                        .putExtra(ApplicationConstants.ADD_ACTIVITY_TITLE, "Address Book"));
                break;
            case 2:
                startActivity(new Intent(HomeActivity.this, OrderHistoryActivity.class));
                break;
            case 3:
                // startActivity(new Intent(HomeActivity.this, AboutUsActivity.class));
                break;
            case 4:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=com.google.android.apps.plus");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                // startActivity(new Intent(HomeActivity.this, AboutUsActivity.class));
                break;
            case 5:
                startActivity(new Intent(HomeActivity.this, AboutUsActivity.class));
                break;
            default:
                startActivity(new Intent(HomeActivity.this, ContactUsActivity.class));
                break;

        }
    }


    public void onClickHandler(View view) {
        switch (view.getId())
        {
            case R.id.home_activity_bottom:
                Intent intent = new Intent(HomeActivity.this,MyOrderActivity.class);
                context.startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof CategoryListResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((CategoryListResponseModel) model);
            }
        } else if (model instanceof WeeklyDishResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((WeeklyDishResponseModel) model);
            }
        } else if (model instanceof AddItemToCartResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                getCartDetail();
            }
        } else if (model instanceof GetCartDetailResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                updateModel((GetCartDetailResponseModel) model);
            }
        }
    }

    public void moveMenuScreen(int pos) {
        Intent i = new Intent(this, MenuDetailsActivity.class);
        i.putExtra("POS", pos);
        startActivity(i);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void onclickHandler(View v) {
        switch (v.getId()) {
            case R.id.add_to_order:
                Toast.makeText(this, "clicked", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        HomeActivity.this);

                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View deleteDialogView = factory.inflate(
                        R.layout.add_to_cart_layout, null);
                final AlertDialog deleteDialog = new AlertDialog.Builder(HomeActivity.this).create();
                deleteDialog.setView(deleteDialogView);

                ImageButton addBtnHalfPrice = (ImageButton) deleteDialogView.findViewById(R.id.addBtnHalfPrice);
                ImageButton minusBtnHalfPrice = (ImageButton) deleteDialogView.findViewById(R.id.minusBtnHalfPrice);
                ImageButton addBtnFullPrice = (ImageButton) deleteDialogView.findViewById(R.id.addBtnFullPrice);
                ImageButton minusBtnFullPrice = (ImageButton) deleteDialogView.findViewById(R.id.minusBtnFullPrice);

                final TextView halfPriceTextView = (TextView) deleteDialogView.findViewById(R.id.halfPriceTextView);
                final TextView fullPriceTextView = (TextView) deleteDialogView.findViewById(R.id.fullPriceTextView);

                if (weeklyDishItem.get(0).HalfPrice != null) {
                    halfPriceTextView.setText("Half Rs " + weeklyDishItem.get(0).HalfPrice);
                }
                if (weeklyDishItem.get(0).FullPrice != null) {
                    fullPriceTextView.setText("Full Rs " + weeklyDishItem.get(0).FullPrice);
                }
                final TextView halfPriceQuantityTextView = (TextView) deleteDialogView.findViewById(R.id.halfPriceQuantityTextView);
                halfPriceQuantityTextView.setText(String.valueOf(halfPriceQuantityText));

                final TextView fullPriceQuantityTextView = (TextView) deleteDialogView.findViewById(R.id.fullPriceQuantityTextView);
                fullPriceQuantityTextView.setText(String.valueOf(fullPriceQuantityText));

                TextView addToOrderBtn = (TextView) deleteDialogView.findViewById(R.id.addToOrderBtn);

                addBtnHalfPrice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        halfPriceQuantityText = halfPriceQuantityText + 1;
                        halfPriceQuantityTextView.setText(String.valueOf(halfPriceQuantityText));
                    }
                });
                minusBtnHalfPrice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (halfPriceQuantityText > 0) {
                            halfPriceQuantityText = halfPriceQuantityText - 1;
                            halfPriceQuantityTextView.setText(String.valueOf(halfPriceQuantityText));
                        }
                    }
                });
                addBtnFullPrice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fullPriceQuantityText = fullPriceQuantityText + 1;
                        fullPriceQuantityTextView.setText(String.valueOf(fullPriceQuantityText));
                    }
                });
                minusBtnFullPrice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (fullPriceQuantityText > 0) {
                            fullPriceQuantityText = fullPriceQuantityText - 1;
                            fullPriceQuantityTextView.setText(String.valueOf(fullPriceQuantityText));
                        }
                    }
                });

                addToOrderBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addToCart(AppPreferences.getUserEmail(prefs), weeklyDishItem.get(0).Id, String.valueOf(fullPriceQuantityText), "1", weeklyDishItem.get(0).FullPrice);
                        deleteDialog.dismiss();
                    }
                });
                deleteDialog.show();
                break;
            default:
                break;
        }
    }

        private void EditBottomDialog(String dishItemValue, String priceTvValue) {
            bottomLyt.setVisibility(View.VISIBLE);
            if (priceTvValue != null)
                totalPriceTv.setText(priceTvValue);
            if (dishItemValue != null)
                totalItemTv.setText(new StringBuilder().append(dishItemValue).append(" dishes in your order ").toString());
    }

    public void addToCart(String uid, String menuId, String quantity, String size, String price) {
        AddItemToCartProvider provider = new AddItemToCartProvider(getAddToCartRequest(uid, menuId, quantity, size, price));
        provider.setCallback((HomeActivity) context);
        provider.appContext = context;
        PDHViewUtils.getInstance().showProgress(context);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    private AddItemToCartRequestModel getAddToCartRequest(String uid, String menuId, String quantity, String size, String price) {
        return new AddItemToCartRequestModel(uid, menuId, quantity, size, price);
    }

    @Override
    public void onClick(View view) {

    }
}

