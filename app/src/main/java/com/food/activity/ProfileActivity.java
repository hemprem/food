package com.food.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import com.food.core.service.PDHAPIDataModelBase;
import com.food.core.service.PDHAPIProviderCallback;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHViewUtils;
import com.food.login.model.LoginRequestModel;
import com.food.login.model.LoginResponseModel;
import com.food.login.provider.LoginProvider;
import com.food.profile.model.EditUserProfileRequestModel;
import com.food.profile.model.EditUserProfileResponseModel;
import com.food.profile.model.GetUserProfileRequestModel;
import com.food.profile.model.GetUserProfileResponseModel;
import com.food.profile.model.SetProfileImageRequestModel;
import com.food.profile.model.SetProfileImageResponseModel;
import com.food.profile.provider.EditUserProfileProvider;
import com.food.profile.provider.GetUserProfileProvider;
import com.food.profile.provider.SetProfileImageProvider;
import com.food.util.AppPreferences;
import com.food.util.ApplicationUtil;
import com.food.R;
import com.food.util.ImageLoader;
import com.food.util.UiUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Author by hemendra.kumar on 1/8/2016.
 */
public class ProfileActivity extends AppCompatActivity implements PDHAPIProviderCallback, View.OnClickListener {

    private static final String TAG = "LoginActivity";
    private Toolbar mToolbar;
    SharedPreferences preferences;
    EditText edtName;
    EditText edtDOB;
    EditText edtEmail;
    EditText edtMobNo;
    Button btn;
    int action;
    private Uri videoUri,photoUri,PhotoUri;
    private String imageMimeType, imageDataString;
    private final int ALBUM =0;
    private final int REQUEST_CODE_TAKE_PHOTO = 4;
    private final int REQUEST_CODE_CHOOSE_PHOTO = 5;
    private String photoPath;
    private ImageLoader imageLoader;
    private ImageView addAlbumPhoto;
    private Bitmap photoBitmap;
    private ImageView imgView;
    private ImageButton toolbarBtnEdit;
    private ImageView profileFadeImageView;
    private ImageView imageView;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText("My Profile");
        titleTv.setGravity(Gravity.CENTER_HORIZONTAL);

        prefs= PreferenceManager.getDefaultSharedPreferences(this);
        ImageButton toolbarBtnEdit = (ImageButton) findViewById(R.id.toolbarBtnEdit);
        edtName =(EditText)findViewById(R.id.activity_profile_full_name_editText);
        edtDOB =(EditText)findViewById(R.id.activity_profile_dob_editText);
        edtEmail =(EditText)findViewById(R.id.activity_profile_emailId_editText);
        edtMobNo =(EditText)findViewById(R.id.activity_profile_mobileNo_editText);
        btn =(Button)findViewById(R.id.activity_profile_change_password_btn);
        btn.setOnClickListener(this);

        imageView= (ImageView)findViewById(R.id.activity_profile_ImgView);
        imageView.setImageBitmap(ApplicationUtil.roundCornerImage(BitmapFactory.decodeResource(getResources(), R.mipmap.log), 50));
        imageView.setOnClickListener(this);

        profileFadeImageView = (ImageView)findViewById(R.id.activity_profile_fade_ImgView);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        getUserProfile(getUserProfileRequest());
        action = 0;
        edtDOB.setEnabled(false);
        edtEmail.setEnabled(false);
        edtMobNo.setEnabled(false);
        edtName.setEnabled(false);
        toolbarBtnEdit.setOnClickListener(this);

       // imgView =(ImageView)findViewById(R.id.img_not_visible);
       /* imageLoader = new ImageLoader(ProfileActivity.this,ProfileActivity.class.getName());
        imageLoader.setRounded(false);
        imageLoader.setScaleToSmallSizeFalse();*/

        if (Build.VERSION.SDK_INT >= 23) {
            //do your check here
            if(!isStoragePermissionGranted()){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            }
        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
       /* grantResultsif(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }*/

        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    private void getUserProfile(GetUserProfileRequestModel loginRequest) {
        GetUserProfileProvider provider = new GetUserProfileProvider(loginRequest);
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    private GetUserProfileRequestModel getUserProfileRequest() {
        GetUserProfileRequestModel request = null;
        request = new GetUserProfileRequestModel("3");
        return request;
    }


    private void editUserProfile(EditUserProfileRequestModel loginRequest) {
        EditUserProfileProvider provider = new EditUserProfileProvider(loginRequest);
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    private EditUserProfileRequestModel editUserProfileRequest() {
        EditUserProfileRequestModel request = null;
        request = new EditUserProfileRequestModel("3",edtName.getText().toString(),edtMobNo.getText().toString(),edtDOB.getText().toString());

        return request;
    }

    private void updateUserProfileImage(SetProfileImageRequestModel loginRequest) {
        SetProfileImageProvider provider = new SetProfileImageProvider(loginRequest);
        provider.setCallback(this);
        provider.appContext = this;
        PDHViewUtils.getInstance().showProgress(this);
        provider.getData(PDHAppState.getInstance().loadFromServer);
    }

    private SetProfileImageRequestModel setProfileImageRequest() {
        SetProfileImageRequestModel request = null;
        request = new SetProfileImageRequestModel("3",imageDataString);

        return request;
    }



    public void changePassword(){
        Intent i = new Intent(ProfileActivity.this,ChangePasswordActivity.class);
        startActivityForResult(i,10);
    }

    //@Override
 /* protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 100 ){

        }
    }*/

    public void DisplayUserProfileModel(GetUserProfileResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
         edtName.setText(model.Result.Record.Name);
         edtMobNo.setText(model.Result.Record.Mobile);
         edtDOB.setText(model.Result.Record.DOB);
         //edtEmail.setText("");
        }
    }
    public void UpdateUserProfileModel(EditUserProfileResponseModel model) {
        if(model.Result.ErrorCode.equals("0")){
            action = 0;
            btn.setText("Change Password");
            edtDOB.setEnabled(false);
            edtEmail.setEnabled(false);
            edtMobNo.setEnabled(false);
            edtName.setEnabled(false);
        }
    }

    public void onclickHandler(View view) {

        switch (view.getId()) {
            case R.id.activity_profile_change_password_btn:
                if(action==0){
                 changePassword();
                }else{
                 editUserProfile(editUserProfileRequest());
                }
                break;
            case R.id.activity_profile_ImgView:
                btn.setText("Edit Profile");
                edtDOB.setEnabled(true);
                edtDOB.setFocusable(true);
                edtEmail.setEnabled(true);
                edtEmail.setFocusable(true);
                edtMobNo.setEnabled(true);
                edtMobNo.setFocusable(true);
                edtName.setEnabled(true);
                edtName.setFocusable(true);
                break;
            default:
                break;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_profile_change_password_btn:
                if(action==0){
                    changePassword();
                }else{
                    editUserProfile(editUserProfileRequest());
                }
                break;
            case R.id.activity_profile_ImgView:
              /*  action=1;
                btn.setText("Edit Profile");
                edtDOB.setEnabled(true);
                edtEmail.setEnabled(true);
                edtMobNo.setEnabled(true);
                edtName.setEnabled(true);*/

                EditAlbumDialog dialog = new EditAlbumDialog(ProfileActivity.this, R.style.DialogSlideAnim,ALBUM);
                dialog.show();

                break;
            case R.id.toolbarBtnEdit:
                action=1;
                btn.setText("Edit Profile");
                edtDOB.setEnabled(true);
                edtEmail.setEnabled(true);
                edtMobNo.setEnabled(true);
                edtName.setEnabled(true);

                break;
            default:
                break;
        }

    }

    @Override
    public void onResponse(String errorString, PDHAPIDataModelBase model) {
        if (errorString != null) {
            PDHViewUtils.getInstance().hideProgress();
            PDHViewUtils.showError(this, "Unable to Process", errorString);

        } else if (model instanceof GetUserProfileResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                DisplayUserProfileModel((GetUserProfileResponseModel) model);
            }
        }else if (model instanceof EditUserProfileResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                UpdateUserProfileModel((EditUserProfileResponseModel) model);
            }
        }else if (model instanceof SetProfileImageResponseModel) {
            if (errorString == null) {
                PDHViewUtils.getInstance().hideProgress();
                //UpdateUserProfileModel((EditUserProfileResponseModel) model);
            }
        }
    }

    /**
     * open video camera to take picture.
     */
    private void openPhotoCamera()
    {

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {

            long captureTime = System.currentTimeMillis();

            photoPath = Environment.getExternalStorageDirectory() + "/MYAPP" + captureTime + ".jpg";

            String strVideoPrompt = "Capture Photo";
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(Intent.createChooser(cameraIntent, strVideoPrompt), REQUEST_CODE_TAKE_PHOTO);
        }
        else{

            Toast.makeText(getApplicationContext(),
                    "Sorry there is a problem accessing your SDCard, " +
                            "please select a picture from your gallery instead.", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * open camera roll to pick photo.
     */
    private void openPhotoGallery()
    {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_CODE_CHOOSE_PHOTO);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
        {
            if(requestCode == REQUEST_CODE_CHOOSE_PHOTO){
                try
                {

                    Uri vid = data.getData();
                    String selectedImagePath = getPath(vid);
                    //Bitmap bitmapUsingPath = BitmapFactory.decodeFile(selectedImagePath);
                    //Bitmap bitmapUsingPath = imageLoader.decodeFile(new File(getRealPathFromURI(vid)));
                    Bitmap photoBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), vid);
                    photoUri = vid;


                    BitmapDrawable ob = new BitmapDrawable(getResources(), photoBitmap);
                    imageView.setImageResource(0);
                    imageView.setBackgroundResource(0);
                    //imageView.setBackgroundDrawable(ob);

                    profileFadeImageView.setImageResource(0);
                    profileFadeImageView.setBackgroundResource(0);
                    profileFadeImageView.setBackgroundDrawable(ob);

                    int imageWidth = imageView.getWidth();
                    int imageHeight = imageView.getHeight();

                    Bitmap _bitmap = ThumbnailUtils.extractThumbnail(photoBitmap, imageWidth, imageHeight);
                    imageView.setImageBitmap(ApplicationUtil.roundCornerImage(_bitmap, 50));




                    //addAlbumPhoto = imgView;
                    //Uri vid = data.getData();
                    //photoUri = vid;

                    //--------------Get Bitmap Using Uri---------------------------//
					/*String imagePath = photoUri.getPath();
					Bitmap  bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(vid));
					int imageWidth = addAlbumPhoto.getWidth();
					int imageHeight = addAlbumPhoto.getHeight();
					Bitmap _bitmap = ThumbnailUtils.extractThumbnail(bitmap, imageWidth, imageHeight);*/

                    //-----------Get Image Mime Type Using Uri------------------------//
                    ContentResolver cR = this.getContentResolver();
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    imageMimeType= mime.getExtensionFromMimeType(cR.getType(photoUri));

                    //addAlbumPhoto.setImageBitmap(_bitmap);

					/*File file1 = new File(imagePath);
					Log.d(LOG_TAG,"file1"+file1+"get Absolute Path"+file1.getAbsolutePath());
				    FileInputStream imageInFile = new FileInputStream(file1.getAbsolutePath());
				    Log.d(LOG_TAG,"imageInFile"+imageInFile);
		            byte imageData[] = new byte[(int) file1.length()];
		        	Log.d(LOG_TAG,"imageData"+imageData);
		            imageInFile.read(imageData);
		            file = file1.getAbsolutePath();
					 */

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    photoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] imageData = stream.toByteArray();

                    // Converting Image byte array into Base64 String
                    imageDataString = UiUtils.encodeImage(imageData);
                   // uploadPhoto();
                   // Log.d(LOG_TAG,"imageDataString"+imageDataString);
                    updateUserProfileImage(setProfileImageRequest());
                }
                catch (Exception e)
                {
                    videoUri = null;
                    Toast.makeText(ProfileActivity.this, "This file format is not supported.", Toast.LENGTH_LONG	).show();
                    e.printStackTrace();
                }
                catch (Throwable e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                };
            }else if(requestCode == REQUEST_CODE_TAKE_PHOTO ){
                try
                {
                    //addAlbumPhoto = imageView;
                    photoBitmap = (Bitmap) data.getExtras().get("data");
                    PhotoUri = getImageUri(this,photoBitmap);


                    BitmapDrawable ob = new BitmapDrawable(getResources(), photoBitmap);

                    imageView.setImageResource(0);
                    imageView.setBackgroundResource(0);
                    //imageView.setBackgroundDrawable(ob);



                    profileFadeImageView.setImageResource(0);
                    profileFadeImageView.setBackgroundResource(0);
                    profileFadeImageView.setBackgroundDrawable(ob);


                    int imageWidth = imageView.getWidth();
                    int imageHeight = imageView.getHeight();

                    Bitmap _bitmap = ThumbnailUtils.extractThumbnail(photoBitmap, imageWidth, imageHeight);

                    imageView.setImageBitmap(ApplicationUtil.roundCornerImage(_bitmap, 50));

                    /*Bitmap circle = getCroppedBitmap(_bitmap);
					_bitmap.recycle();
					_bitmap = null;*/


                    //-----------Get Image Mime Type Using Uri------------------------//
                    ContentResolver cR = this.getContentResolver();
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    imageMimeType= mime.getExtensionFromMimeType(cR.getType(PhotoUri));

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    photoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] imageData = stream.toByteArray();

                    // Converting Image byte array into Base64 String
                    imageDataString = UiUtils.encodeImage(imageData);
                    // uploadPhoto();
                    updateUserProfileImage(setProfileImageRequest());
                }
                catch (Exception e)
                {
                    videoUri = null;
                    Toast.makeText(ProfileActivity.this, "This file format is not supported.", Toast.LENGTH_LONG	).show();

                    e.printStackTrace();
                }

            }
        }
    }

    public String getRealPathFromURI(Uri contentUri)
    {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }
    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

		/* String[] projection = new String[] {MediaStore.Images.Thumbnails._ID, MediaStore.Images.Thumbnails.IMAGE_ID};
	     Cursor thumbnails = getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, projection, null, null, null);
	     // Then walk thru result and obtain imageId from records
	      for (thumbnails.moveToFirst(); !thumbnails.isAfterLast(); thumbnails.moveToNext()) {
	      String imageId = thumbnails.getString(thumbnails.getColumnIndex(Thumbnails.IMAGE_ID));
	      // Request image related to this thumbnail
	      String[] filePathColumn = { MediaStore.Images.Media.DATA };
	      Cursor cursor = getContentResolver().query(uri, filePathColumn, MediaStore.Images.Media._ID + "=?", new String[] {imageId}, null);
	      if (cursor != null && cursor.moveToFirst()) {
	          // Your file-path will be here
	          String filePath = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));
	          return filePath;
	      }
	  }*/
        //return null;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }








    class EditAlbumDialog extends Dialog implements View.OnClickListener
    {
        /**
         * @param context
         * @param theme
         */
        private int dialogType;
        public EditAlbumDialog(Context context, int theme, int _dialogType)
        {
            super(context, theme);
            dialogType=_dialogType;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.dialog_video_option);

            View buttonCancel = findViewById(R.id.cancelBtn);
            View capturePhoto = findViewById(R.id.cpaturePhotoBtn);
            View chooseFromListing = findViewById(R.id.chooseFromListingBtn);

            if(dialogType==ALBUM){
                ((Button)capturePhoto).setText("Capture Photo");
                ((Button)chooseFromListing).setText(getResources().getString(R.string.choose_from_listing));
            }/*else if(dialogType==PHOTO){
					((Button)uploadAlbum).setText(getResources().getString(R.string.take_photo));
					((Button)updateAlbum).setText(getResources().getString(R.string.choose_from_gallery));
				}*/

            //set click listener
            buttonCancel.setOnClickListener(EditAlbumDialog.this);
            capturePhoto.setOnClickListener(EditAlbumDialog.this);
            chooseFromListing.setOnClickListener(EditAlbumDialog.this);
        }

        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.cpaturePhotoBtn:
                    dismiss();
                    openPhotoCamera();
                    break;
                case R.id.chooseFromListingBtn:
                    openPhotoGallery();
                    dismiss();
                    break;
                case R.id.cancelBtn:
                    dismiss();
                    break;
                default:
                    break;
            }
        }
    }
}