package com.food.placeOrder.provider;

import android.util.Log;

import com.food.R;
import com.food.core.service.PDHAPIProviderBase;
import com.food.core.utils.PDHAppState;
import com.food.core.utils.PDHConstants;
import com.food.core.utils.PDHStringUtil;
import com.food.core.utils.PDHURLUtils;
import com.food.placeOrder.model.PlaceOrderRequestModel;
import com.food.placeOrder.model.PlaceOrderResponseModel;
import com.food.placeOrder.service.PlaceOrderAPIService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.InputStream;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Author by hemendra.kumar on 4/8/2016.
 */
public class PlaceOrderProvider extends PDHAPIProviderBase {


    public static boolean lastRequestSuccess = true;

    private PlaceOrderRequestModel placeOrderRequest;

    public String jsonName = null;

    public PlaceOrderProvider(PlaceOrderRequestModel placeOrderRequestModel) {
        this.placeOrderRequest = placeOrderRequestModel;
    }

    @Override
    public void loadDataFromServer() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appContext.getString(R.string.host_url))
                .addConverterFactory(converterFactory)
                .build();
        PlaceOrderAPIService service = retrofit.create(PlaceOrderAPIService.class);
        String jsonData = PDHURLUtils.JSONData(placeOrderRequest, PlaceOrderRequestModel.class, PDHAppState.getInstance().encodeInputJSON);
        Log.d("API:Req:getMTNDetails", jsonData);
        Call<PlaceOrderResponseModel> call = service.getPlaceOrder(placeOrderRequest);

        call.enqueue(new Callback<PlaceOrderResponseModel>() {
            @Override
            public void onResponse(Response<PlaceOrderResponseModel> response, Retrofit var) {
                PlaceOrderResponseModel model = response.body();
                String errorString = null;
                if (null == model) {
                    errorString = "INVALID Response:Check Connection";
                } else if (!PDHConstants.SERVER_SUCCESS_CODE.equals(model.Result.ErrorCode)) {
                    Log.e("API:Resp:getMTNDetails", model.toString());

                    errorString = PDHStringUtil.createServerErrorString(model.Result.ErrorCode, model.Result.ErrorMsg, null);
                    model = null;
                }
                callback.onResponse(errorString, model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("app", "error" + t.toString());
                PlaceOrderProvider.lastRequestSuccess = false;
                callback.onResponse(t.toString(), null);
            }
        });
    }



    @Override
    public void loadDataFromCache() {
        try {
            InputStream is = appContext.getAssets().open("jsons/getMTNDetails.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String bufferString = new String(buffer);
            Gson gson = new Gson();
            PlaceOrderResponseModel model = gson.fromJson(bufferString, PlaceOrderResponseModel.class);
            callback.onResponse(null, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
