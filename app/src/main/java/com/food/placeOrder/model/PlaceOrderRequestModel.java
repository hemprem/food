package com.food.placeOrder.model;

/**
 * Author by hemendra.kumar on 4/8/2016.
 */
public class PlaceOrderRequestModel {

    private String uid;
    private String total_price;
    private String discount;
    private String comment;
    private String address;
    private orders orders;

    private class orders {
        private String cart_id;
        private String menu_id;
        private String quantity;
        private String size;
        private String price;

        private  orders(String cart_id,String menu_id,String quantity,String size,String price ){
            this.cart_id=cart_id;
            this.menu_id=menu_id;
            this.quantity=quantity;
            this.size=size;
            this.price=price;
        }
    }



    public PlaceOrderRequestModel(String uid, String total_price, String discount,String comment, String address,orders orders ) {
        this.uid =uid;
        this.total_price=total_price;
        this.discount =discount;
        this.comment = comment;
        this.address = address;
        this.orders=orders;
    }
}
